export const measuresList = (index) => {
    switch (index) {
        case 1: return 'за услугу';
        case 2: return 'за урок';
        case 3: return 'за 1 м²';
        case 4: return 'за 1 м³';
        case 5: return 'за 1 пог. метр';
        case 6: return 'за 1 шт.';
        case 7: return 'за 1 метр';
        case 8: return 'за кг.';
        case 9: return 'за 1 км';
        case 10: return 'за 45 мин.';
        case 11: return 'за 1 час +';
        case 12: return 'за 1 сутки';
        case 13: return 'за 1 неделю';
        case 14: return 'за 1 месяц';
    }
}
