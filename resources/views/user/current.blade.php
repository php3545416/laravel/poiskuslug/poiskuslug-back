@extends('layouts.app')
@section('content')
    <section class="home-section-items">
        <div class="container">
            <div class="page-col-inner">
                <div class="page-col-left s-895">
                    <div class="section__items">
                        <user-item :user="{{ $user }}" :header-phone=true :all-adverts=true></user-item>
                    </div>
                </div>
                <div class="page-col-right s-300">
                    @if(!empty($BannersPages[0]->path))
                        <div class="vertical-img-bnr">
                            <a target="_blank" href="{{$BannersPages[0]->link}}">
                                <img src="{{$BannersPages[0]->path}}"/>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
