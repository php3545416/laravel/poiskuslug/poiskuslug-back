@extends('layouts.app')

@section('content-top')
    <section class="home-section-one">
        <div class="container">
       <div class="home-title-block">
            <h1 class="home-section-one__title">@if (! empty($dataPageDefault->h1)) {{ $dataPageDefault->h1 }} @endif</h1>
            <div class="home-section-one__user-count">{{ $users->totalString }}</div>
        </div>
            <div class="home-section-one__text">@if (! empty($dataPageDefault->text)) {!! $dataPageDefault->text !!} @endif</div>
        </div>
    </section>
    <section class="home-section-categories" id="home-categories">
        <div class="container">
            <div class="categories__section__block">
                <ul>
                    @if (! empty($subCategories))
                        @foreach($subCategories as $subCategory)
                            <li>
                                <a href="{{ $URLTreeDTO->getUrl() . $subCategory->url }}/">{{ $subCategory->name }}</a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="home-section-items">
        <div class="container">
            <div class="page-col-inner">
                <div class="page-col-left s-895">
                    <users-list
                        region="{{ $URLTreeDTO->getRegion()->url }}"
                        category="{{ $URLTreeDTO->getCategory()->url }}"
                        sub-category="{{ $URLTreeDTO->getSubCategory() ? $URLTreeDTO->getSubCategory()->url : '' }}"
                        advert="{{ $URLTreeDTO->getAdvert() ? $URLTreeDTO->getAdvert()->url : '' }}"
                    ></users-list>
                </div>
                <div class="page-col-right s-300">
                    <div class="section__items-side">
                        @if(! empty($BannersPages[0]->path))
                            <div class="vertical-img-bnr">
                                <a target="_blank" href="{{$BannersPages[0]->link}}">
                                    <img src="{{$BannersPages[0]->path}}"/>
                                </a>
                            </div>
                        @endif
                        @if(! empty($sidePanelAdverts))
                            <ul>
                                @foreach($sidePanelAdverts as $advert)
                                    <li>
                                        <a href="{{ $URLTreeDTO->getUrl() . $advert->url }}/">{{ $advert->name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
