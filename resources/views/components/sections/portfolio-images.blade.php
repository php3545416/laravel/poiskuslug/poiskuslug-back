@props([
    'user'
])
@if(! empty($user->images))
    <div class="section__items-item-gallery">
        <div class="swiper item-gallery">
            <div class="swiper-wrapper">
                @foreach($user->images as $photo)
                    <div class="swiper-slide">
                        <a href="{{ asset("storage" . str_replace("_small","_big", $photo->path)) }}" data-fancybox="gallery">
                            <img src="{{ asset("storage/" . $photo->path) }}"/>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endif
