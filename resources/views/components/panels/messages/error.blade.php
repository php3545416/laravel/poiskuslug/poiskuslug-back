@props(['messages'])
<div {{ $attributes }}>
    <div class="form-validation-errors alert alert-danger" id="error-messages">
        @foreach ($messages as $message)
            <div>{{ $message }}</div>
        @endforeach
    </div>
</div>
