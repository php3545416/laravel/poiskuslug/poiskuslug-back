@extends('layouts.app')

@section('content')

    <section class="home-section-items">
        <div class="container">
            <div class="page-col-inner">
                <div class="page-col-left s-895">

                    <div class="section-select-region">
                        @if (count($breadsRegion) == 1)
                            <h1 class="page__section_title_two">{{ $dataPageDefault->title }}</h1>
                        @else
                            <h1 class="page__section_title_two">{{ $dataPageDefault->title }} </h1>
                        @endif

                        @if (! empty($breadsRegion))
                            <div class="regions-breadcrumbs">
                                @foreach ($breadsRegion as $Link => $Name)
                                    @if ($Link != '')
                                        <a href="{{ $Link }}">{{ $Name }}</a>
                                    @else
                                        / {{ $Name }}
                                    @endif
                                @endforeach
                            </div>
                        @endif
                        
                        <div class="regions__list__items">
                            @if (! empty($regions))
                                @foreach ($regions as $location)
                                    <div class="regions__list__items-col">
                                        <div class="regions__list__items-item @if ($location['favorite']) active @endif">
                                            @if (! $location['parent_id'])
                                                <a href="{{ route('region.city', $location->id ) }}/">{{ $location->name }}</a>
                                            @else
                                                <a href="/{{ $location->url }}/">{{ $location->name }}</a>
                                            @endif
                                            @if ($location->users_count !== 0)
                                            <span class="count">{{ $location->users_count }}</span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                        </div>
                    </div>
                </div>

                <div class="page-col-right s-300">
                    @if(!empty($BannersPages[0]->path))
                        <div class="vertical-img-bnr">
                            <a target="_blank" href="{{$BannersPages[0]->link}}">
                                <img src="{{$BannersPages[0]->path}}"/>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
