@extends('layouts.admin')

@section('content')
    <main class="main-page-inner col-8 p-0">
        @if ($item)
            <form id="form-delete" name="form-delete" action="{{ route('admin.category.delete',$item->id) }}/"
                  method="post" class="form-horizontal">
                @csrf
            </form>
        @endif
        <form action="{{ route('admin.category.save') }}/" method="post" class="form-horizontal">
            <div class="card">
                <div class="card-header">
                    @if ($item)
                        Редактирование категории {{ $item->name }}
                    @else
                        Добавление категории
                    @endif
                </div>
                <div class="card-body card-block">
                    @csrf
                    @if (! empty($item->id))
                        <input type="hidden" name="item_id" value="{{ $item->id }}"/>
                    @endif

                    <input type="hidden" name="service" value="0">
                    @if ((request()->get('sub') == 1 || (! empty($item->parent_id) && $item->parent_id > 0)))
                        <div class="form-group">
                            <label for="parent_id" class="form-control-label">Основная категория</label>
                            <select class="form-control" name="parent_id">
                                @if ($categoryList)
                                    @foreach ($categoryList as $category)
                                        <option value="{{ $category->id }}"
                                                @if (! empty($item->parent_id) && $category->id == $item->parent_id) selected @endif>{{ $category->name }}
                                        </option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    @else
                        <input type="hidden" name="parent_id" value="0"/>
                    @endif

                    <div class="form-group">
                        <label class="form-control-label">
                            Название категории</label>
                        <input
                            type="text"
                            class="form-control"
                            name="name"
                            @if(!empty($item->name)) value="{{ $item->name }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">URL</label>
                        <input
                            type="text"
                            class="form-control"
                            name="url"
                            @if(! empty($item->url)) value="{{ $item->url }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">URL иконки</label>
                        <input
                            type="text"
                            class="form-control"
                            name="icon"
                            @if(! empty($item->icon)) value="{{ $item->icon }}" @endif
                        >
                    </div>

                    <div class="card-header">
                        SEO настройки категории
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Title</label>
                        <input
                            type="text"
                            class="form-control"
                            name="title"
                            @if(! empty($item->title)) value="{{ $item->title }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Description</label>
                        <input
                            type="text"
                            class="form-control"
                            name="description"
                            @if (! empty($item->description)) value="{{ $item->description }}"@endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">H1</label>
                        <input
                            type="text"
                            class="form-control"
                            name="h1"
                            @if (! empty($item->h1)) value="{{$item->h1}}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Хлебные крошки</label>
                        <input
                            type="text"
                            class="form-control"
                            name="name_bread"
                            @if (! empty($item->name_bread)) value="{{ $item->name_bread }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Описание категории / SEO-текст</label>
                        <textarea
                            class="form-control"
                            name="text"
                        >@if(! empty($item->text)){{ $item->text }}@endif</textarea>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Мастер (1)</label>
                        <input
                            type="text"
                            class="form-control"
                            name="master_1"
                            @if (! empty($item->master_1)) value="{{ $item->master_1 }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Мастера (2,3,4)</label>
                        <input
                            type="text"
                            class="form-control"
                            name="master_2"
                            @if (! empty($item->master_2)) value="{{ $item->master_2 }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Мастеров (5,6,7,8,9,0)</label>
                        <input
                            type="text"
                            class="form-control"
                            name="master_3"
                            @if (! empty($item->master_3)) value="{{ $item->master_3 }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="switch switch-3d switch-success mr-3">
                            <input
                                type="checkbox"
                                name="favorite"
                                class="switch-input"
                                @if (! empty($item->favorite) && $item->favorite) checked @endif
                            >
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <label class="form-control-label"> Важная категория</label>
                    </div>

                    <div class="form-group">
                        <label class="switch switch-3d switch-success mr-3">
                            <input
                                    type="checkbox"
                                    name="display_at_home"
                                    class="switch-input"
                                    @if (! empty($item->display_at_home) && $item->display_at_home) checked @endif
                            >
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <label class="form-control-label"> Выводить на главной</label>
                    </div>

                    <div class="text-black">
                        {city} {city.in} {region} {region.in} {master.text} {total.m}
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Сохранить
                    </button>
                    <button type="submit" form="form-delete" class="btn btn-danger m-l-15">
                        Удалить
                    </button>
                </div>
            </div>
        </form>
    </main>
@endsection
