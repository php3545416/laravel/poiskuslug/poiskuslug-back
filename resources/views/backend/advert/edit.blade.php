@extends('layouts.admin')

@section('content')
    <main class="main-page-inner col-8 p-0">
        @if($advert)
            <form id="form-delete" name="form-delete" action="{{ route('admin.advert.delete', $advert->id) }}/"
                  method="post" class="form-horizontal">
                @csrf
                @method('DELETE')
            </form>
        @endif
        <form action="{{ route('admin.advert.update', $advert->id) }}/" method="post" class="form-horizontal">
            @csrf
            @method('POST')
            <div class="card">
                <div class="card-header">
                    Редактирование услуги {{ $advert->name }}
                </div>
                <div class="card-body card-block">

                    <x-panels.messages.form_validation_errors />

                    <div class="form-group">
                        <label class="form-control-label">Категория услуги</label>
                        <select class="form-control" name="category_id">
                            @if ($tree)
                                @foreach($tree as $category)
                                    <option
                                        disabled
                                        value="{{ old('category_id', $category->id) }}"
                                        @if(! empty(old('category_id', $advert->category_id)) && $category->id == old('category_id', $advert->category_id)) selected @endif
                                    >
                                        {{ $category->name }}
                                    </option>

                                    @if ($category->subCategories)
                                        @foreach ($category->subCategories as $subCategory)
                                            <option
                                                value="{{ old('category_id', $subCategory->id) }}"
                                                @if (! empty(old('category_id', $advert->category_id)) && $subCategory->id == old('category_id', $advert->category_id)) selected @endif
                                            >
                                                ---{{ $subCategory->name }}
                                            </option>
                                        @endforeach
                                    @endif

                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Настройка цены</label>
                        <select class="form-control" name="measure">
                            @if($measureList)
                                @foreach($measureList as $key => $measure)
                                    <option
                                        value="{{ old('measure', $key) }}"
                                        @if(! empty(old('measure', $advert->measure)) && $key == old('measure', $advert->measure)) selected @endif
                                    >
                                        {{ $measure }}
                                    </option>
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Название услуги</label>
                        <input
                            type="text"
                            class="form-control"
                            name="name"
                            @if(! empty(old('name', $advert->name))) value="{{ old('name', $advert->name) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">URL</label>
                        <input
                            type="text"
                            class="form-control"
                            name="url"
                            @if(! empty(old('url', $advert->url))) value="{{ old('url', $advert->url) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">URL иконки</label>
                        <input
                            type="text"
                            class="form-control"
                            name="icon"
                            @if(! empty(old('icon', $advert->icon))) value="{{ old('icon', $advert->icon) }}" @endif
                        >
                    </div>

                    <div class="card-header">
                        SEO настройки категории
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Title</label>
                        <input
                            type="text"
                            class="form-control"
                            name="title"
                            @if(! empty(old('title', $advert->title))) value="{{ old('title', $advert->title) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Description</label>
                        <input
                            type="text"
                            class="form-control"
                            name="description"
                            @if (! empty(old('description', $advert->description))) value="{{ old('description', $advert->description) }}"@endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">H1</label>
                        <input
                            type="text"
                            class="form-control"
                            name="h1"
                            @if (! empty(old('h1', $advert->h1))) value="{{ old('h1', $advert->h1) }}"@endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Хлебные крошки</label>
                        <input
                            type="text"
                            class="form-control"
                            name="name_bread"
                            @if (! empty(old('name_bread', $advert->name_bread))) value="{{ old('name_bread', $advert->name_bread) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Описание категории / SEO-текст</label>
                        <textarea
                            class="form-control"
                            name="text"
                        >@if (! empty(old('text', $advert->text))) {{ old('text', $advert->text) }} @endif</textarea>
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Мастер (1)</label>
                        <input
                            type="text"
                            class="form-control"
                            name="master_1"
                            @if (! empty(old('master_1', $advert->master_1))) value="{{ old('master_1', $advert->master_1) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Мастера (2,3,4)</label>
                        <input
                            type="text"
                            class="form-control"
                            name="master_2"
                            @if (! empty(old('master_2', $advert->master_2))) value="{{ old('master_2', $advert->master_2) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="form-control-label">Мастеров (5,6,7,8,9,0)</label>
                        <input
                            type="text"
                            class="form-control"
                            name="master_3"
                            @if (! empty(old('master_3', $advert->master_3))) value="{{ old('master_3', $advert->master_3) }}" @endif
                        >
                    </div>

                    <div class="form-group">
                        <label class="switch switch-3d switch-success mr-3">
                            <input
                                type="checkbox"
                                name="popular"
                                class="switch-input"
                                @checked(old('popular', $advert->popular))
                            >
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <label class="form-control-label"> Популярная услуга</label>
                    </div>
                    <div class="form-group">
                        <label class="switch switch-3d switch-success mr-3">
                            <input
                                type="checkbox"
                                name="show_col"
                                class="switch-input"
                                @checked(old('show_col', $advert->show_col))
                            >
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <label class="form-control-label"> Смотрите также (боковая колонка)</label>
                    </div>
                    <div class="form-group">
                        <label class="switch switch-3d switch-success mr-3">
                            <input
                                    type="checkbox"
                                    name="can_be_selected"
                                    class="switch-input"
                                    @checked(old('can_be_selected', $advert->can_be_selected))
                            >
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                        <label class="form-control-label"> Можно выбрать пользователю</label>
                    </div>

                    <div class="text-black">
                        {city} {city.in} {region} {region.in} {master.text} {total.m}
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-success">Сохранить
                    </button>
                    <button type="submit" form="form-delete" class="btn btn-danger m-l-15">
                        Удалить
                    </button>
                </div>
            </div>
        </form>
    </main>
@endsection
