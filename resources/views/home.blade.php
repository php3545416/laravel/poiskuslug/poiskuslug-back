@extends('layouts.app')

@section('content-top')
    <section class="home-section-one">
        <div class="container">
            <h1 class="home-section-one__title">@if(! empty($dataPageDefault->h1)) {{ $dataPageDefault->h1 }} @endif</h1>
            <div class="home-section-one__text">@if(! empty($dataPageDefault->text)){!! $dataPageDefault->text !!}@endif
            </div>
        </div>
    </section>
    <section class="home-section-categories" id="home-categories">
        <div class="container">
            <div class="categories__section__block">
                <ul>
                    @if(! empty($categories))
                        @foreach($categories as $category)
                            <li>
                                <a href="@if (! empty($CityRegion)) /{{ $CityRegion->url }}/@if(! empty($category->parent)){{ $category->parent->url }}/@endif{{ $category->url }}/ @else /moskva/{{ $category->url }}/ @endif">
                                    <img src="/images/{{$category->icon}}" alt=""/><span>{{$category->name}}</span>
                                </a>
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </section>
@endsection
@section('content')
    <section class="home-section-items">
        <div class="container">
            <div class="page-col-inner">
                <div class="page-col-left s-895">
                    <home-adverts-loop></home-adverts-loop>
                </div>
                <div class="page-col-right s-300">
                    @if(!empty($BannersPages[0]->path))
                        <div class="vertical-img-bnr">
                            <a target="_blank" href="{{$BannersPages[0]->link}}">
                                <img src="{{$BannersPages[0]->path}}"/>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@endsection
