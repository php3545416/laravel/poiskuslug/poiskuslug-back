<?php

return [
    'base_url' => env('HI_CALL_SERVICE_API_BASE_URL'),
    'api_key' => env('HI_CALL_SERVICE_API_KEY'),
];
