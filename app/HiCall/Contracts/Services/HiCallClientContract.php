<?php

namespace App\HiCall\Contracts\Services;

use Illuminate\Http\Client\HttpClientException;

interface HiCallClientContract
{
    /**
     * @param string $phone
     * @return array
     * @throws HttpClientException
     */
    public function sendVoiceCallRequest(string $phone): array;

    /**
     * @param string $callId
     * @return array
     * @throws HttpClientException
     */
    public function getCallInfoRequest(string $callId): array;
}
