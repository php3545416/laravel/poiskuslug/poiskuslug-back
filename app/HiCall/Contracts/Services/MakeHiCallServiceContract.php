<?php

namespace App\HiCall\Contracts\Services;

use App\HiCall\Exceptions\HiCallBaseException;

interface MakeHiCallServiceContract
{
    /**
     * @param string $phone
     * @return array
     * @throws HiCallBaseException
     */
    public function makeVoiceCall(string $phone): array;

    /**
     * @param string $code
     * @param string $callId
     * @return bool
     * @throws HiCallBaseException
     */
    public function confirmCode(string $code, string $callId): bool;
}
