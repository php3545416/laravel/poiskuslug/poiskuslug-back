<?php

namespace App\HiCall\Providers;

use App\HiCall\Contracts\Services\HiCallClientContract;
use App\HiCall\Contracts\Services\MakeHiCallServiceContract;
use App\HiCall\Services\HiCallClient;
use App\HiCall\Services\MakeHiCallService;
use Illuminate\Support\ServiceProvider;

class HiCallServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/hi-call.php', 'hi-call'
        );

        $this->app->singleton(
            HiCallClientContract::class,
            fn () => $this->app->make(HiCallClient::class, [
                'baseUrl' => config('hi-call.base_url'),
                'apiKey' => config('hi-call.api_key'),
            ])
        );

        $this->app->singleton(
            MakeHiCallServiceContract::class,
            MakeHiCallService::class
        );
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        $this->publishes([
            __DIR__ . '/../config/hi-call.php' => $this->app->configPath('hi-call.php'),
        ], 'hi-call');

    }
}
