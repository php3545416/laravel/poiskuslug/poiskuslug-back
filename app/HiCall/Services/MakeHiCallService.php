<?php

namespace App\HiCall\Services;

use App\HiCall\Contracts\Services\HiCallClientContract;
use App\HiCall\Contracts\Services\MakeHiCallServiceContract;

class MakeHiCallService implements MakeHiCallServiceContract
{
    public function __construct(private readonly HiCallClientContract $hiCallClient)
    {
    }

    public function makeVoiceCall(string $phone): array
    {
        return $this->hiCallClient->sendVoiceCallRequest($phone);
     }

    public function confirmCode(string $code, string $callId): bool
    {
        $response = $this->hiCallClient->getCallInfoRequest($callId);

        return $code === $response['code'];
    }
}
