<?php

namespace App\HiCall\Services;

use App\HiCall\Contracts\Services\HiCallClientContract;
use App\HiCall\Exceptions\HiCallBaseException;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;

class HiCallClient implements HiCallClientContract
{
    public function __construct(
        private readonly string $baseUrl,
        private readonly string $apiKey,
    ) {
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $headers
     * @param array $data
     * @return Response
     * @throws HiCallBaseException
     */
    protected function sendRequest(
        string $method,
        string $url,
        array $headers = [],
        array $data = []
    ): Response {
        /** @var Response $response */
        $response = Http::baseUrl($this->baseUrl)
            ->withHeaders($headers)
            ->{$method}($url, $data)
        ;

        $responseBody = json_decode($response->body(), true);

        if ($responseBody['status'] === 'error') {
            throw new HiCallBaseException($response);
        }

        return $response;
    }

    public function sendVoiceCallRequest(string $phone): array
    {
        return $this->sendRequest(
            'get',
            "/voice/{$this->apiKey}/{$phone}"
        )->json();
    }

    public function getCallInfoRequest(string $callId): array
    {
        return $this->sendRequest(
            'get',
            "/info/{$callId}"
        )->json();
    }
}
