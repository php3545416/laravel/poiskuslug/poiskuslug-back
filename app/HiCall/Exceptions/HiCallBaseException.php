<?php

namespace App\HiCall\Exceptions;

use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\Client\Response;

class HiCallBaseException extends HttpClientException
{
    /**
     * The response instance.
     *
     * @var Response
     */
    public Response $response;

    /**
     * Create a new exception instance.
     *
     * @param  Response  $response
     * @return void
     */
    public function __construct(Response $response)
    {
        parent::__construct(
            $this->prepareMessage($response),
            400
        );

        $this->response = $response;
    }

    /**
     * Prepare the exception message.
     *
     * @param  Response  $response
     * @return string
     */
    protected function prepareMessage(Response $response)
    {
        $responseBody = json_decode($response->body(), true);

        return BaseHiCallExceptionMessages::fromName($responseBody['error']);
    }
}
