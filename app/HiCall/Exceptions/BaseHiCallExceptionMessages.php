<?php

namespace App\HiCall\Exceptions;

enum BaseHiCallExceptionMessages: string
{
    const PLEASE_MESSAGE = 'Пожалуйста, обратитесь в службу поддержки сайта.';
    const REPEAT_LATER_MESSAGE = 'Пожалуйста, повторите позже.';

    case api_key_format = 'API-ключ не соответствует формату UUID. ' . self::PLEASE_MESSAGE;
    case phone_format = 'Номер телефона не соответствует формату. ';
    case api_key_not_found = 'В запросе указан некорректный (не существующий) API-ключ. ' . self::PLEASE_MESSAGE;
    case project_inactive = 'Проект, к которому относится указанный API-ключ, не активен. ' . self::PLEASE_MESSAGE;
    case account_blocked = 'Аккаунт заблокирован.' . self::PLEASE_MESSAGE;
    case balance_error = 'На балансе недостаточно средств для совершения звонка. ' . self::PLEASE_MESSAGE;
    case antispam_in_queue = 'Предыдущий звонок еще на стадии обработки. Пожалуйста, дождитесь его.';
    case antispam_rate_limit = 'Звонки можно совершать не чаще, чем раз в минуту. ' . self::REPEAT_LATER_MESSAGE;
    case api_rate_limit = 'Превышено ограничение (10 запросов в секунду) на попытки отправки запроса. ' . self::REPEAT_LATER_MESSAGE;
    case api_rate_limit_ip = 'Превышено ограничение (10 раз в секунду) на попытки отправки запроса. ' . self::REPEAT_LATER_MESSAGE;
    case phone_rate_limit_hour = 'Превышено ограничение (20 раз) на количество звонков в час на ваш номер телефона. ' . self::REPEAT_LATER_MESSAGE;
    case phone_rate_limit_day = 'Превышено ограничение (100 раз) на число звонков на один номер телефона в сутки. ' . self::REPEAT_LATER_MESSAGE;
    case phone_in_blacklist = 'Данный номер телефона добавлен в чёрный список, отправлять звонки на него невозможно. ' . self::PLEASE_MESSAGE;

    public static function fromName(string $name): string
    {
        return match($name) {
            self::api_key_format->name => self::api_key_format->value,
            self::phone_format->name => self::phone_format->value,
            self::api_key_not_found->name => self::api_key_not_found->value,
            self::project_inactive->name => self::project_inactive->value,
            self::account_blocked->name => self::account_blocked->value,
            self::balance_error->name => self::balance_error->value,
            self::antispam_in_queue->name => self::antispam_in_queue->value,
            self::antispam_rate_limit->name => self::antispam_rate_limit->value,
            self::api_rate_limit->name => self::api_rate_limit->value,
            self::api_rate_limit_ip->name => self::api_rate_limit_ip->value,
            self::phone_rate_limit_hour->name => self::phone_rate_limit_hour->value,
            self::phone_rate_limit_day->name => self::phone_rate_limit_day->value,
            self::phone_in_blacklist->name => self::phone_in_blacklist->value,
            default => null
        };
    }
}
