<?php

namespace App\Http\Resources;

use Illuminate\Http\Response;

class BaseResource
{
    public function __construct(
        private int $statusCode = 200,
    ) {
    }

    public function success($data): Response
    {
        $response = response([
            'status' => 'success',
            'data' => $data,
        ]);

        return $response->setStatusCode($this->statusCode);
    }

    public function error($data, int $status, string $errorCode = 'unspecified', string $message = 'Ошибка сервера'): Response
    {
        return response([
            'status' => 'error',
            'data' => [
                'code' => $errorCode,
                'message' => $message,
                'content' => $data,
            ],
        ])->setStatusCode($status);
    }

    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }
}
