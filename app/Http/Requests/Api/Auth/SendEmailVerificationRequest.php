<?php

namespace App\Http\Requests\Api\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class SendEmailVerificationRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'email', 'max:255', 'unique:users,email,'. request()->user()->id],
        ];
    }

    public function messages(): array
    {
        return [
            'email.required' => 'Поле обязательно для заполнения',
            'email.email' => 'Неверный формат электронной почты',
            'email.max' => 'Адрес электронной почты не должен превышать 255 символов',
            'email.unique' => 'Такой адрес электронной почты уже занят',
        ];
    }
}
