<?php

namespace App\Http\Requests\Api\Auth;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;


class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'phone' => ['required', 'max:11', 'min:11'],
            'name' => ['required', 'string', 'max:300'],
            'email' => ['nullable', 'email', 'max:255', 'unique:'. User::class],
        ];
    }

    public function messages(): array
    {
        return [
            'phone.required' => 'Введите номер телефона, чтобы получить код',
            'phone.max' => 'Номер телефона должен содержать 11 цифр',
            'phone.min' => 'Номер телефона должен содержать 11 цифр',
            'name.required' => 'Имя должно быть заполнено',
            'name.string' => 'Имя должно содержать только буквы',
            'name.max' => 'Имя не должно превышать 300 символов. Символов введено - ' . strlen($this->name),
            'email.email' => 'Неверный формат электронной почты',
            'email.max' => 'Адрес электронной почты не должен превышать 255 символов',
            'email.unique' => 'Такой адрес электронной почты уже занят',
        ];
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'phone' => str_replace(['+', ' ', '(', ')', '_'], '', $this->phone)
        ]);
    }
}
