<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'code' => 'required|numeric',
            'callId' => 'required|string',
            'phone' => '',
        ];
    }

    public function messages(): array
    {
        return [
            'code.required' => 'Вы не ввели код',
            'code.numeric' => 'Код должен состоять только из цифр',
        ];
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'phone' => str_replace(['+', ' ', '(', ')', '_'], '', $this->phone)
        ]);
    }
}
