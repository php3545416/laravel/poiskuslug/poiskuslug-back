<?php

namespace App\Http\Requests\Api\Auth;

use Illuminate\Foundation\Http\FormRequest;

class MakeCallRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'phone' => 'required|string|max:11|min:11',
        ];
    }

    public function messages(): array
    {
        return [
            'phone.required' => 'Введите номер телефона, чтобы получить код',
            'phone.max' => 'Номер телефона должен содержать 11 цифр',
            'phone.min' => 'Номер телефона должен содержать 11 цифр',
        ];
    }

    public function prepareForValidation(): void
    {
        $this->merge([
            'phone' => str_replace(['+', ' ', '(', ')', '_'], '', $this->phone)
        ]);
    }
}
