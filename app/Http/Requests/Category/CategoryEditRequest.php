<?php

namespace App\Http\Requests\Category;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class CategoryEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }

    public function messages()
    {
        return [];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'favorite' => $this->has('favorite'),
            'display_at_home' => $this->has('display_at_home'),
            'url' => ! $this->has('url') ? Str::slug($this->name, '-') : $this->url,
            'icon' => ! $this->has('icon') ? Str::slug($this->icon, '-') : $this->icon,
        ]);
    }
}
