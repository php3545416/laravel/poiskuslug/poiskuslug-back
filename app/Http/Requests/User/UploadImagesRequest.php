<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UploadImagesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'file' =>  'userphotos|mimes:jpg,jpeg,png|max:10240'
        ];
    }

    public function messages(): array
    {
        return [
            'file.userphotos' => 'Можно загрузить не больше 20 изображений',
            'file.mimes' => 'Доступные форматы: jpg, gif, png',
            'file.max'   => 'Максимальны размер фото 10 МБ'
        ];
    }
}
