<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class SetDiscountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'discount' => '',
            'discount_text' => ! empty($this->discount) ? 'required' : ''
        ];
    }

    public function messages()
    {
        return [
            'discount.required' => 'Поле Размер скидки обязательно для заполнения',
            'discount_text.required' => 'Вы не заполнили условия предоставления скидки или подарка'
        ];
    }
}