<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserAboutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'work_experience' =>  'string',
            'about_text' => 'string|max:400'
        ];
    }

    public function messages(): array
    {
        return [
            'about_text.max' => 'Размер текста должен быть не больше 400 символов. Символов введено - ' . strlen($this->about_text),
        ];
    }
}
