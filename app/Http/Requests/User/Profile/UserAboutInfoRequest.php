<?php

namespace App\Http\Requests\User\Profile;

use App\Enums\Sex;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;

class UserAboutInfoRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'birthday' => 'nullable|date_format:Y-m-d|before:today',
            'sex' => 'nullable|in:' . implode(',', Sex::values()),
            'email' => ['nullable', 'email', 'max:255', 'unique:users,email,'. request()->user()->id],
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Поле ФИО обязательно для заполнения',
            'birthday.date_format' => 'Некорректный формат даты рождения',
            'birthday.before' => 'Некорректная дата рождения',
            'sex.in' => 'Некорректное значение пола',
            'email.email' => 'Неверный формат электронной почты',
            'email.max' => 'Адрес электронной почты не должен превышать 255 символов',
            'email.unique' => 'Такой адрес электронной почты уже занят',
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge([
            'birthday' => Carbon::createFromFormat('d/m/Y', $this->birthday)->format('Y-m-d'),
            'email' => empty($this->email) ? null : $this->email,
        ]);
    }
}
