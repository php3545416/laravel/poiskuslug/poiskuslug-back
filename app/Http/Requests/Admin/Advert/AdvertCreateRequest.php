<?php

namespace App\Http\Requests\Admin\Advert;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class AdvertCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required',
            'url' => 'required',
            'popular' => '',
            'show_col' => '',
            'favorite' => '',
            'icon' => '',
            'master_1' => '',
            'master_2' => '',
            'master_3' => '',
            'text' => '',
            'name_bread' => '',
            'h1' => '',
            'description' => '',
            'title' => '',
            'category_id' => 'required|integer',
            'measure' => '',
            'can_be_selected' => '',
        ];
    }

    public function messages(): array
    {
        return [
            'name.required' => 'Название услуги - обязательное поле',
            'url.required' => 'URL-строка услуги - обязательное поле',
            'category_id.required' => 'Не выбрана категория услуги'
        ];
    }

    public function prepareForValidation(): static
    {
        return $this->merge([
            'popular' => $this->has('popular'),
            'show_col' => $this->has('show_col'),
            'favorite' => $this->has('favorite'),
            'can_be_selected' => $this->has('can_be_selected'),
            'url' => ! $this->has('url') ? Str::slug($this->name, '-') : $this->url,
            'icon' => ! $this->has('icon') ? Str::slug($this->name, '-') : $this->icon,
        ]);
    }
}
