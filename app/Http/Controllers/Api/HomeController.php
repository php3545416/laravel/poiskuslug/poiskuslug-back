<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\UsersServiceContract;
use App\DTO\UsersFilterDTO;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        private readonly UsersServiceContract $usersService,
    ) {
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $usersFilterDTO = (new UsersFilterDTO())
            ->setRegionId($request->session()->get('city'))
            ->setPage($request->get('page', 1))
            ->setPerPage(2)
            ->setRelations(['categories', 'adverts', 'images'])
            ->setOrderBy('created_at')
            ->setOrderByDirection('desc');

        $usersAdverts = $this->usersService->getUsersByRegion($usersFilterDTO);

        return new JsonResponse([
            'success' => true,
            'adverts' => $usersAdverts,
        ]);
    }
}
