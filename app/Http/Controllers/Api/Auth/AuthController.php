<?php

namespace App\Http\Controllers\Api\Auth;

use App\Contracts\Services\UserNotificationServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\DTO\UsersFilterDTO;
use App\HiCall\Contracts\Services\MakeHiCallServiceContract;
use App\HiCall\Exceptions\HiCallBaseException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\MakeCallRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Requests\Api\Auth\SendEmailVerificationRequest;
use App\Http\Resources\Api\Auth\AuthResource;
use App\Http\Resources\BaseResource;
use App\Models\User;
use App\Notifications\EmailVerificationNotification;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    protected string $redirectTo = '/profile/';

    public function __construct(
        private readonly MakeHiCallServiceContract $makeHiCallService,
        private readonly UsersServiceContract $usersService
    ) {}

    public function makeCall(MakeCallRequest $request): Response
    {
        try {
            $response = $this->makeHiCallService->makeVoiceCall(
                $request->validated('phone')
            );
        } catch (HiCallBaseException $exception) {
            return (new BaseResource())->error(
                [],
                $exception->getCode(),
                'clientError',
                $exception->getMessage()
            );
        }

        return (new BaseResource())->success(
            data: AuthResource::make($response)
        );
    }

    /**
     * @throws HiCallBaseException
     */
    public function login(LoginRequest $request): RedirectResponse|Response
    {
        if ($this->makeHiCallService->confirmCode(
            $request->validated('code'),
            $request->validated('callId')
        )) {
            $user = $this->usersService->getUserByPhone(
                (new UsersFilterDTO())->setPhone($request->validated('phone'))
            );

            if ($user) {
                Auth::login($user);

                return redirect(route('user.profile'));
            } else {
                return (new BaseResource())->success(
                    data: ['message' => 'notFound']
                );
            }
        };

        return (new BaseResource())->success(
            data: ['message' => 'notValidCode']
        );
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws ValidationException
     */
    public function register(RegisterRequest $request): RedirectResponse
    {
        $user = User::create([
            'name' => $request->validated('name'),
            'phone' => $request->validated('phone'),
            'email' => $request->validated('email'),
            'confirm_phone' => 1,
            'user_type' => 1,
            'status' => 1,

        ]);

        if ($user && $user->email) {
            Notification::send($user, new EmailVerificationNotification($user));
        }

        Auth::login($user);

        return redirect(route('user.profile'));
    }

    public function sendEmailVerification(
        SendEmailVerificationRequest $request,
        UserNotificationServiceContract $userNotificationService
    ): Response {
        $status = $userNotificationService->sendVerificationMail($request->user(), $request->validated('email'));

        if ($status) {
            return (new BaseResource())->success(
                data: []
            );
        }

        return (new BaseResource())->error(
            data: [],
            status: 422,
        );
    }

    public function verifyEmail(string $id): RedirectResponse
    {
        /** @var User $user */
        $user = User::findOrFail($id);

        $successMessage = 'Почта уже была подтверждена ранее!';

        if (! $user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();

            $successMessage = 'Почта успешно подтверждена!';
        }

        Auth::login($user);

        return redirect(route('user.profile'))->with(['success' => $successMessage]);
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect(RouteServiceProvider::HOME);
    }
}
