<?php

namespace App\Http\Controllers\Api;

use App\Contracts\Services\URLTreeCollectorServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\DTO\UsersFilterDTO;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ListingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        private readonly UsersServiceContract $usersService,
        private readonly URLTreeCollectorServiceContract $URLTreeCollectorService
    ) {
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function categoryPage(Request $request): JsonResponse
    {
        $URLTreeDTO = $this->URLTreeCollectorService->collectURLTree(
            $request->get('region'),
            $request->get('category')
        );

        $usersFilterDTO = (new UsersFilterDTO())
            ->setRegionId($request->session()->get('city'))
            ->setCategoryId($URLTreeDTO->getCategory()->id)
            ->setRelations(['adverts', 'images', 'categories'])
            ->setOrderBy('last_profile_up_at')
            ->setOrderByDirection('desc')
            ->setPage($request->get('page', 1))
            ->setPerPage(2);

        $users = $this->usersService->getUsersByRegionAndCategory($usersFilterDTO, $URLTreeDTO->getCategory());

        return new JsonResponse([
            'success' => true,
            'adverts' => $users,
        ]);
    }

    public function subCategoryPage(Request $request): JsonResponse
    {
        $URLTreeDTO = $this->URLTreeCollectorService->collectURLTree(
            $request->get('region'),
            $request->get('category'),
            $request->get('subCategory')
        );

        $usersFilterDTO = (new UsersFilterDTO())
            ->setRegionId($request->session()->get('city'))
            ->setSubCategoryId($URLTreeDTO->getSubCategory()->id)
            ->setRelations(['adverts', 'images', 'categories'])
            ->setOrderBy('last_profile_up_at')
            ->setOrderByDirection('desc')
            ->setPage($request->get('page', 1))
            ->setPerPage(2);

        $users = $this->usersService->getUsersByRegionAndSubCategory($usersFilterDTO, $URLTreeDTO->getSubCategory());

        return new JsonResponse([
            'success' => true,
            'adverts' => $users
        ]);
    }

    public function advertPage(Request $request): JsonResponse
    {
        $URLTreeDTO = $this->URLTreeCollectorService->collectURLTree(
            $request->get('region'),
            $request->get('category'),
            $request->get('subcategory'),
            $request->get('advert')
        );

        $usersFilterDTO = (new UsersFilterDTO)
            ->setRegionId($request->session()->get('city'))
            ->setAdvertId($URLTreeDTO->getAdvert()->id)
            ->setRelations(['categories', 'adverts', 'images'])
            ->setOrderBy('last_profile_up_at')
            ->setOrderByDirection('desc')
            ->setPage($request->get('page'))
            ->setPerPage(2);

        $users = $this->usersService->getUsersByRegionAndAdvert($usersFilterDTO, $URLTreeDTO->getAdvert());

        return new JsonResponse([
            'success' => true,
            'adverts' => $users,
        ]);
    }
}
