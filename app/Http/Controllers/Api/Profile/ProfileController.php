<?php

namespace App\Http\Controllers\Api\Profile;

use App\Contracts\Services\UpdateUserProfileServiceContract;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        private readonly UpdateUserProfileServiceContract $usersService,
    ) {
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function upProfile(Request $request): JsonResponse
    {
        $user = $this->usersService->upProfile(Auth::user()->id);

        return new JsonResponse([
            'success' => true,
            'user' => $user
        ]);
    }
}
