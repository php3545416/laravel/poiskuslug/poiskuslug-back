<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Contracts\Services\CategoriesServiceContract;
use App\Contracts\Services\SeoPagesServiceContract;
use App\Contracts\Services\URLTreeCollectorServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\DTO\CategoriesFilterDTO;
use App\DTO\UsersFilterDTO;
use App\Models\Category;
use App\Models\Image;
use App\Models\SeoPage;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Region;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class AdvertsController extends Controller
{
    public function __construct(
        private readonly CategoriesServiceContract $categoriesService,
        private readonly UsersServiceContract $usersService,
        private readonly URLTreeCollectorServiceContract $URLTreeCollectorService,
        private readonly AdvertsRepositoryContract $advertsRepository,
        private readonly SeoPagesServiceContract $seoPagesService
    ) {}

    public function currentAdvert(
        string $region,
        string $category,
        string $subCategory,
        string $advert,
        Request $request
    ) {
        $URLTreeDTO = $this->URLTreeCollectorService->collectURLTree(
            $region,
            $category,
            $subCategory,
            $advert
        );

        $usersFilterDTO = (new UsersFilterDTO)
            ->setRegionId($request->session()->get('city'))
            ->setAdvertId($URLTreeDTO->getAdvert()->id)
            ->setRelations(['categories', 'adverts'])
            ->setOrderBy('last_profile_up_at')
            ->setOrderByDirection('desc')
            ->setPage($request->get('page', 1))
            ->setPerPage(2);

        $users = $this->usersService->getUsersByRegionAndAdvert($usersFilterDTO, $URLTreeDTO->getAdvert());

        $seo = SeoPage::make([
            'title' => $URLTreeDTO->getAdvert()->title,
            'h1' => $URLTreeDTO->getAdvert()->h1,
            'description' => $URLTreeDTO->getAdvert()->description,
            'text' => $URLTreeDTO->getAdvert()->text,
        ]);

        $dataPage = $this->seoPagesService->fillDataSeoPage(
            $seo,
            $URLTreeDTO->getRegion(),
            $users->total(),
            $URLTreeDTO->getAdvert()
        );

        return view('adverts.category', [
            'URLTreeDTO' => $URLTreeDTO,
            'users' => $users,
            'dataPageDefault'=> $dataPage,
        ]);
    }

    public function categoryPage(Request $request): View
    {
        $URLTreeDTO = $this->URLTreeCollectorService->collectURLTree(
            $request->route('region'),
            $request->route('category')
        );

        $categoriesFilterDTO = (new CategoriesFilterDTO())
            ->setParentId($URLTreeDTO->getCategory()->id)
            ->setRegionId($request->session()->get('city'));

        $subCategories = $this->categoriesService->getSubCategories($categoriesFilterDTO);

        $usersFilterDTO = (new UsersFilterDTO())
            ->setLimit(20)
            ->setRegionId($request->session()->get('city'))
            ->setCategoryId($URLTreeDTO->getCategory()->id)
            ->setRelations(['adverts'])
            ->setOrderBy('last_profile_up_at')
            ->setOrderByDirection('desc')
            ->setPage($request->get('page', 1))
            ->setPerPage(2);

        $users = $this->usersService->getUsersByRegionAndCategory($usersFilterDTO, $URLTreeDTO->getCategory());

        $seo = SeoPage::make([
            'title' => $URLTreeDTO->getCategory()->title,
            'h1' => $URLTreeDTO->getCategory()->h1,
            'description' => $URLTreeDTO->getCategory()->description,
            'text' => $URLTreeDTO->getCategory()->text,
        ]);

        $dataPage = $this->seoPagesService->fillDataSeoPage(
            $seo,
            $URLTreeDTO->getRegion(),
            $users->total(),
            $URLTreeDTO->getCategory()
        );

        return view('adverts.category', [
            'URLTreeDTO' => $URLTreeDTO,
            'users' => $users,
            'subCategories'   => $subCategories,
            'dataPageDefault' => $dataPage,
        ]);
    }

    public function subCategoryPage(
        Request $request,
    ): View {
        $URLTreeDTO = $this->URLTreeCollectorService->collectURLTree(
            $request->route('region'),
            $request->route('category'),
            $request->route('subcategory')
        );

        if (! $URLTreeDTO->getSubCategory() || ! $URLTreeDTO->getCategory()) {
            if(! $URLTreeDTO->getSubCategory()){
                return $this->currentAdvert(
                    $request->route('region'),
                    $request->route('category'),
                    '',
                    $request->route('subcategory'),
                    $request
                );
                exit();
            }
            abort(404);
        }

        $popularAdverts = collect();
        $sidePanelAdverts = collect();

        if ($URLTreeDTO->getSubCategory()) {
            $popularAdverts = $this->advertsRepository->getPopularAdvertsByCategory(
                $URLTreeDTO->getSubCategory()->id,
                $URLTreeDTO->getRegion()->id,
            );

            $sidePanelAdverts = $this->advertsRepository->getSidePanelAdvertsByCategory(
                $URLTreeDTO->getSubCategory()->id,
                $URLTreeDTO->getRegion()->id,
            );
        }

        $usersFilterDTO = (new UsersFilterDTO())
            ->setRegionId($request->session()->get('city'))
            ->setSubCategoryId($URLTreeDTO->getSubCategory()->id)
            ->setRelations(['adverts'])
            ->setOrderBy('last_profile_up_at')
            ->setOrderByDirection('desc')
            ->setPage($request->get('page', 1))
            ->setPerPage(2);

        $users = $this->usersService->getUsersByRegionAndSubCategory($usersFilterDTO, $URLTreeDTO->getSubCategory());

        $seo = SeoPage::make([
            'title' => $URLTreeDTO->getSubCategory()->title,
            'h1' => $URLTreeDTO->getSubCategory()->h1,
            'description' => $URLTreeDTO->getSubCategory()->description,
            'text' => $URLTreeDTO->getSubCategory()->text,
        ]);

        $dataPage = $this->seoPagesService->fillDataSeoPage(
            $seo,
            $URLTreeDTO->getRegion(),
            $users->total(),
            $URLTreeDTO->getSubCategory()
        );

        return view(
            'adverts.category', [
                'users' => $users,
                'URLTreeDTO' => $URLTreeDTO,
                'dataPageDefault' => $dataPage,
                'subCategories'  => $popularAdverts,
                'sidePanelAdverts'  => $sidePanelAdverts,
            ]
        );
    }

    //Список городов по региону

    public function getCitysByRegion(Request $request)
    {
        $Citys = Region::GetAllCitys($request->input('regionID'));
        return response()->json(['success' => $Citys]);
    }
    //Список подкатегория по категории

    public function getCategoriesByParent(Request $request)
    {
        $Categories = Category::GetAllCategories($request->input('parentID'));
        return response()->json(['success' => $Categories]);
    }
    //Проверяем изображение к требованиям

    public function UploadImage(Request $request)
    {
        return response()->json(['errors' => Image::ImageUploadCheck($request->all())]);
    }
    //Валидация формы для добавления объетка

    private function ValidationAddForm($data)
    {
        return Validator::make(
            $data, [
            'user_name'   => 'required|string|max:190',
            'user_phone'  => 'required|regex:/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/|unique:users,phone,'
                . Auth::user()->id,
            'region'      => 'required|integer',
            'city'        => 'required|integer',
            'title'       => 'required|string|max:190',
            'category'    => 'required|integer',
            //'subcategory' => 'required|integer',
            'description' => 'required|string',
            //'accept'      => 'required|string',
        ], [
                'user_name.required'   => 'Укажите имя',
                'user_phone.required'  => 'Укажите телефон',
                'user_phone.regex'     => 'Введите корректный телефон, например 79221110500',
                'user_phone.unique'    => 'Пользователь с данным телефоном уже зарегистрирован. Используйте другой телефон',
                'region.required'      => 'Укажите регион',
                'region.integer'       => 'Укажите регион',
                'city.required'        => 'Укажите город',
                'city.integer'         => 'Укажите город',
                'title.required'       => 'Заполните название объявления',
                'category.required'    => 'Выберите категорию',
                'category.integer'     => 'Выберите категорию',
                //'subcategory.required' => 'Выберите подкатегорию',
               // 'subcategory.integer'  => 'Выберите подкатегорию',
                'description.required' => 'Заполните название описание',
                //'accept.required'      => 'Вы должны согласиться с условиями',
            ]
        );
    }
    //Показать телефон

    public function getphone(Request $request)
    {

        $AdvertItem = User::where(
            ['id'        => $request->input('item')]
        )->first();

        return response()->json(['success' => !empty($AdvertItem->phone) ?  $AdvertItem->phone : ""]);
    }
    //Лист избранного

    public function UpdateWish(Request $request)
    {
        $ID = $request->input('id');
        if (!empty(Auth::user()->id)) {
            $Advert = DB::table('wishlist')->where('user_id', Auth::user()->id)->where('advert_id', $ID)->first();
            if (!$Advert) {
                DB::table('wishlist')->insert(
                    ['user_id' => Auth::user()->id, 'advert_id' => $ID, 'datetime' => date('Y-m-d H:i:s')]
                );
                return response()->json(['success' => 1]);
            } else {
                DB::table('wishlist')->where('user_id', Auth::user()->id)->where('advert_id', $ID)->delete();
                return response()->json(['success' => 0]);
            }


        } else {
            $Advert = DB::table('wishlist')->where('session_id', session()->getId())->where('advert_id', $ID)->first();
            if (!$Advert) {
                DB::table('wishlist')->insert(
                    ['session_id' => session()->getId(), 'advert_id' => $ID, 'datetime' => date('Y-m-d H:i:s')]
                );
                return response()->json(['success' => 1]);
            } else {
                DB::table('wishlist')->where('session_id', session()->getId())->where('advert_id', $ID)->delete();
                return response()->json(['success' => 0]);
            }


        }
    }
    public function WishList()
    {
        return back();
    }
}
