<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\UsersRepositoryContract;
use App\Contracts\Services\UserImagesServiceContract;
use App\Contracts\Services\UserNotificationServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\Http\Requests\User\Profile\UserAboutInfoRequest;
use App\Http\Requests\User\UploadImagesRequest;
use App\Http\Requests\User\UserAboutRequest;
use App\Http\Resources\BaseResource;
use App\Models\Region;
use App\Models\Image;
use App\Models\SeoPage;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Helpers\UserHelper;
use App\Http\Requests\User\SetDiscountRequest;
use Illuminate\View\View;

class UserController extends Controller
{
    public function __construct(
        private readonly UsersRepositoryContract $usersRepository,
        private readonly UserNotificationServiceContract $userNotificationService,
    ) {
//
//        $this->middleware(
//            function ($request, $next) {
//                if ($request->is('logout')) {
//                    return $next($request);
//                }
//
//                if (!empty(Auth::user()) && Auth::user()->status == 0) {
//                    return redirect(route('user.confirm.page'));
//                }
//
//                if (!empty(Auth::user()->user_type) && Auth::user()->user_type == 2) {
//                    return redirect(route('admin.index'));
//                }
//                return $next($request);
//            }
//        );
    }

    public function currentUser(
        int $id,
        UsersServiceContract $usersService
    ): View {
        $user = User::findOrFail($id);

        if (Auth::user() && Auth::user()->id !== $id) {
            $user->views_total = $user->views_total + 1;
            $user->save();
        }

        $user = $usersService->getUserData($id);

        return view('user.current', ['user' => $user]);
    }

    //Страница с объявлениями пользователя
    public function userProfile()
    {
        $Photos = Image::where('user_id', Auth::user()->id)->get();

        $User = User::where('id', Auth::user()->id)->with('categories')->first();

        $UserCitys = $this->usersRepository->getUserCities(Auth::user()->id);

        $dataPageDefault = SeoPage::make([
            'title' => "Личный кабинет",
            'h1' => '',
            'description' => "Личный кабинет",
            'text' => '',
        ]);

        return view('user.profile.profile', [
            'Photos' => $Photos,
            'User' => $User,
            'dataPageDefault' => $dataPageDefault,
            'UserCitys' => $UserCitys,
        ]);
    }

    //Страница настроек пользователя
    public function userAboutPage(Request $request)
    {
        DB::table('advert_user')
            ->newQuery()
            ->
        return view('user.settings', ['user' => Auth::user()]);
    }

    public function saveUserAboutInfo(
        UserAboutInfoRequest $request,
    ): Response
    {
        return (new BaseResource())->success(
            data: ['user' => $user]
        );
    }

    //Страница регионов пользователя
    public function userRegionsForm()
    {
        $User = User::where('id', Auth::user()->id)->first();

        $Regions = Region::GetAllRegions();

        $UserRegions = DB::table('region_user')
            ->select('city_id')
            ->where('user_id', Auth::user()->id)
            ->get()
            ->keyBy('city_id')
            ->toArray();

        if ($Regions) {
            foreach ($Regions as &$region) {
                $region->citys = Region::GetAllCitys($region->id);
            }
        }

        return view('user.profile.regions', [
            'User' => $User,
            'Regions' => $Regions,
            'UserRegions' => $UserRegions
        ]);
    }

    public function userRegions(Request $request)
    {
        $id = Auth::user()->id;

        DB::table('region_user')->where('user_id', $id)->delete();

        if ($request->get('city')) {
            $inserts = [];
            foreach ($request->get('city') as $key => $region) {
                foreach ($region as $city) {
                    $inserts[] = [
                        'user_id' => $id,
                        'city_id' => $city,
                        'region_id' => $key
                    ];
                }
            }

            DB::table('region_user')->insert($inserts);
        }

        return response()->json([
            'success' => true,
            'alert' => view('notifications.success', ['Title' => "Города", 'Text' => "Список городов успешно сохранен"])->render()
        ]);
    }

    //Редактирование информации о пользователе
    public function userAbout(UserAboutRequest $request)
    {
        $User = User::where('id', Auth::user()->id)->first();

        $User->work_experience = $request->validated('work_experience');
        $User->about_text = $request->validated('about_text');
        $User->save();

        return response()->json([
            'success' => true,
            'alert' => view('notifications.success', ['Title' => "О себе", 'Text' => "Ваши данные успешно сохранены"])->render(),
            'view' => view('user.profile.about',['User'=>$User])->render()
        ]);

    }

    //Редактирование скидки пользователя
    public function userDiscount(SetDiscountRequest $request)
    {
        $User = User::where('id', Auth::user()->id)->first();
        $User->discount = $request->validated()['discount'];
        $User->discount_text = $User->discount === null ? null : $request->validated()['discount_text'];
        $User->save();
        $User = User::where('id', Auth::user()->id)->first();

        return response()->json([
            'success' => true,
            'alert' => view('notifications.success', ['Title' => "Скидка", 'Text' => "Информация о вашей скидке успешно сохранена"])->render(),
            'view' => view('user.profile.discount', ['User' => $User])->render()
        ]);

    }

    //Загрузка и обработка аватара пользователя
    public function uploadUserPhoto(
        Request $request,
        UserImagesServiceContract $userImagesService
    ): JsonResponse {
        $errors  = $this->UserImageUploadCheck($request->all());

        if (! empty($errors) && $errors->fails()) {
            return response()->json([
                'success' => false,
                'errors' => $errors->errors()->toArray(),
                'alert' => view('notifications.error', [
                    'Title' => "Загрузка фотографии",
                    'Errors' => $errors->errors()
                ])->render()
            ]);
        } else {
            $user = $userImagesService->addProfilePhoto(Auth::user()->id, $request->file('file'));

            return response()->json([
                'success' => true,
                'image' => $user->photo,
                'alert' => view('notifications.success', [
                    'Title' => "Загрузка фотографии",
                    'Text' => "Ваша фотография успешно обновлена",
                ])->render()
            ]);
        }
    }

    //Страница портфолио пользователя
    public function userPortfolio(): View
    {
        return view('user.profile.portfolio', [
            'user' => $this->usersRepository->getUserData(Auth::user()->id, ['images'])
        ]);
    }

    public function uploadPortfolio(
        UploadImagesRequest $request,
        UserImagesServiceContract $userImagesService
    ): JsonResponse {
        $errors = Image::ImageUploadCheck($request->all());

        if (! empty($errors) && $errors->fails()) {
            return response()->json([
                'success' => 0,
                'errors' => $errors,
                'alert' => view('notifications.error', ['Title' => "Загрузка портфолио", 'Errors' => $errors->errors()])->render()
            ]);
        } else {
            $image = $userImagesService->linkPortfolioImageToUser(Auth::user()->id, $request->file('file'));

            return response()->json([
                'success' => 1,
                'image_id' => $image->id,
                'image_url' => $image->path,
                'alert' => view('notifications.success', ['Title' => "Загрузка портфолио", 'Text' => "Изображение загружено"])->render()
            ]);
        }
    }

    public function deletePortfolio(Request $request): void
    {
        $Image = Image::where(['user_id' => Auth::user()->id, 'id' => $request->input('image_id')])->first();
        @unlink(public_path('storage')  . $Image->path);
        @unlink(public_path('storage')  . str_replace('_small', '_big', $Image->path));
        Image::where(['user_id' => Auth::user()->id, 'id' => $request->input('image_id')])->delete();
    }


    //TODO Пересмотреть логику изменения, сейчас всё работает
    public function userSettings(Request $request)
    {
        $Errors = $this->ValidationFormEdit($request->all());

        if (!empty($Errors) && $Errors->fails()) {
            return redirect()->back()->withErrors($Errors)->withInput($request->all())->with('UserSettings', 1);
        } else {
            $UserPhone = UserHelper::ClearUserPhone($request->input('phone'));
            $User = User::where(['id' => Auth::user()->id])->first();
            if (($User->phone == $UserPhone)
                || ($User->phone != $UserPhone && $request->input('code')
                    && $request->input('code') != ''
                    && $request->input('code') == $request->session()->get('sms_code'))) {
                $User->update(
                    [
                        'email' => $request->input('email'),
                        'name'  => $request->input('name'),
                        'phone' => $UserPhone,
                    ]
                );
                return redirect(route('user.profile'))->with('success', 'Настройки')->with(
                    'success_text', 'Личные данные сохранены'
                );
            } else {
                if ($User->phone != $UserPhone && !$request->input('code')) {
                    $Code = rand(1111, 9999);
                    $Code = 1111;
                    $request->session()->put('sms_code', $Code);
                    /*$smsaero_api = new SmsaeroApiV2('','');
                     $res = $smsaero_api->flash($Phone, $Code);*/
                }
                if ($User->phone != $UserPhone || $request->input('code')) {
                    $request->session()->put('sms_code_phone_change', $UserPhone);
                }
                if ($request->input('code') && $request->input('code') != $request->session()->get('sms_code')) {
                    return redirect(route('user.settings'))->withErrors(array('code' => "Не верный код"))->withInput(
                        $request->all()
                    )->with('needcodeconfirm', '1')->with('UserSettings', 1);;
                }

                return redirect(route('user.settings'))->withInput($request->all())->with('needcodeconfirm', '1')->with(
                    'UserSettings', 1
                );
            }
        }
    }

    private function validationFormEdit($data)
    {
        return Validator::make(
            $data, [
            // 'email' => 'required|string|email|max:190|unique:users,email,' . Auth::user()->id,
            'name'  => 'required|string|max:190',
            'phone' => 'required|regex:/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/|unique:users,phone,'
                . Auth::user()->id,/*
            'phone' => 'required|regex:/^\s*(?:\+?(\d{1,3}))?([-. (]*(\d{3})[-. )]*)?((\d{3})[-. ]*(\d{2,4})(?:[-.x ]*(\d+))?)\s*$/|unique:users,phone,' . Auth::user()->id,*/
        ], [
                'name.required'      => 'Укажите Имя',
                'email.required'     => 'Укажите свой E-mail',
                'email.email'        => 'Проверьте E-mail',
                'email.unique'       => 'Пользователь с данным email уже зарегистрирован. Используйте другой email',
                'phone.required'     => 'Укажите телефон',
                'phone.regex'        => 'Введите корректный телефон, например 79221110500',
                'phone.unique'       => 'Пользователь с данным телефоном уже зарегистрирован. Используйте другой телефон',
                'password.required'  => 'Укажите пароль',
                'password.min'       => 'Короткий пароль, минимум 6 символов',
                'password.confirmed' => 'Пароли не совпадают. Повторите попытку',
            ]
        );
    }

    //Страница выхода
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function userImageUploadCheck($ImageFile)
    {

        $validator = Validator::make(
            $ImageFile, [
            'file' => 'required|mimes:jpg,jpeg,png,gif|max:10240',
        ], $messages = [
            'mimes' => 'Доступные форматы: jpg, gif, png',
            'max'   => 'Максимальны размер фото 10МБ'
        ]
        );

        return $validator;
    }
}
