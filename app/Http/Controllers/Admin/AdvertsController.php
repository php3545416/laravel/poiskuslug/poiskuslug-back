<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Contracts\Services\AdvertCreateServiceContract;
use App\Contracts\Services\AdvertRemoveServiceContract;
use App\Contracts\Services\AdvertUpdateServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Advert\AdvertCreateRequest;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;


class AdvertsController extends Controller
{
    public function create(
        AdvertsRepositoryContract $advertsRepository
    ): View {
        $tree = Category::GetAllCategories();

        return view('backend.advert.create', [
            'advert' => $advertsRepository->getModel(),
            'tree' => $tree
        ]);
    }

    public function store(
        AdvertCreateRequest $request,
        AdvertCreateServiceContract $advertCreateService
    ): RedirectResponse {
        $advertCreateService->create($request->validated());

        return redirect(route('admin.category.list'))->with('success', "Услуга создана");
    }

    public function edit(
        int $id,
        AdvertsRepositoryContract $advertsRepository
    ): View {
        $tree = Category::GetAllCategories();
        $advert = $advertsRepository->getById($id);

        return view('backend.advert.edit', [
            'advert' => $advert,
            'tree' => $tree
        ]);
    }

    public function update(
        int $id,
        AdvertCreateRequest $request,
        AdvertUpdateServiceContract $advertUpdateService
    ): RedirectResponse {
        $advertUpdateService->update($id, $request->validated());

        return redirect(route('admin.category.list'))->with('success', "Услуга сохранена");
    }

    public function delete(
        int $id,
        AdvertRemoveServiceContract $advertRemoveService
    ): RedirectResponse {
        $advertRemoveService->delete($id);

        return redirect(route('admin.category.list'))->with('success', "Услуга удалена");
    }

}
