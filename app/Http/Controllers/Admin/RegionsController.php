<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Services\RegionsServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Url;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class RegionsController extends Controller
{
    public function __construct(private readonly RegionsServiceContract $regionsService)
    {
    }

    //Список областей
    public function list(Request $request)
    {
        $listItems = $this->regionsService->getAllRegions();

        return view('backend/region/list', ['listItems' => $listItems]);
    }

    //Список городов по области
    public function listCity($Region, Request $request)
    {
        $RegionCurrent = Region::where('id', $Region)->first();
        $listItems = $this->regionsService->getRegionCities($Region);

        return view('backend/region/list', ['listItems' => $listItems, 'Region' => $RegionCurrent]);
    }

    //Форма региона
    public function edit($ID, Request $request)
    {
        $Item = array();
        $RegionsList = $this->regionsService->getAllRegions();
        if ($ID) {
            $Item = Region::where('id', $ID)->first();
        }

        return view('backend/region/edit', ['Item' => $Item, 'RegionsList' => $RegionsList]);
    }

    //Удаление региона
    public function delete($ID, Request $request)
    {
        if ($ID) {
            $Item = Region::where('id', $ID)->first();
            $Item->delete();
            Cache::forget("current_regions_footer");
            return redirect(route('admin.region.list'))->with('success', 'Регион удален');
        } else {
            return redirect(route('admin.region.list'))->with('error', 'Регион не удален');
        }
    }

    //Сохранение региона
    public function save(Request $request)
    {
        if($request['url'])
            $request['url'] = $request->input('url');
        else
            $request['url'] = Url::str2url($request->input('name'));

        $request['favorite'] = $request['favorite'] ? $request['favorite'] : 0;
        $request['public'] = $request['public'] ? $request['public'] : 0;
        $request['footer'] = $request['footer'] ? $request['footer'] : 0;
        Cache::forget("current_regions_footer");
        if ($request->input('item_id')) {
            Region::where('id', $request->input('item_id'))->first()->fill($request->all())->save();
            return redirect(route('admin.region.list'))->with('success', 'Регион сохранен');
        } else {
            Region::create($request->all());
            return redirect(route('admin.region.list'))->with('success', 'Регион добавлен');
        }
    }

}
