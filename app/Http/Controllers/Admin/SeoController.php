<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SeoController extends Controller
{
    //Редактирование сео настроек
    public function edit(Request $request)
    {
        $Items = DB::table('seo_settings')->orderBy('position', 'ASC')->get();
        $Banners = DB::table('banners')->orderBy('id', 'ASC')->get();
        return view('backend/seopages/settings', ['Items' => $Items, 'Banners'=>$Banners]);
    }

    //Сохранение сео настроек
    public function save(Request $request)
    {
        //$Page = first();
        DB::table('seo_settings')->where('id', $request->input('item_id'))->update(
            ['title' => $request->input('title'), 'h1' => $request->input('h1'),
                'text'  => $request->input('text'), 'description' => $request->input('description'),]
        );

        return redirect(route('admin.seo.edit'))->with('success', 'Настройки сохранены');
    }


    //Сохранение баннеров
    public function bannersSave(Request $request)
    {
        if($request->input('link')){
            foreach( $request->input('link') as $key=>$Item){
                DB::table('banners')->where('id', $key)->update(
                    ['link' =>$Item,'path' =>$request->input('path')[$key]]
                );
            }
        }

        return redirect(route('admin.seo.edit'))->with('success', 'Баннеры сохранены');
    }

}
