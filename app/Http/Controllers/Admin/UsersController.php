<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Contracts\Services\AdvertCreateServiceContract;
use App\Contracts\Services\AdvertRemoveServiceContract;
use App\Contracts\Services\AdvertUpdateServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Advert\AdvertCreateRequest;
use App\Http\Requests\Admin\User\UserEditRequest;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;


class UsersController extends Controller
{
    //Список пользователей
    public function list(Request $request)
    {
        $Filter = array();

        if ($request->input('search')) {
            $Filter['id'] = $request->input('search');
            $Filter['email'] = $request->input('search');
        }

        $listItems = User::GetUsersList($Filter);

        return view('backend/users/list', ['listItems' => $listItems]);
    }

    //Форма пользователя
    public function edit($ID, Request $request)
    {
        $Item = User::where('id', $ID)->first();

        return view('backend/users/edit', ['Item' => $Item]);
    }

    //Сохранение пользователя
    public function save(UserEditRequest $request)
    {
        if ($request->input('item_id')) {
            User::where('id', $request->input('item_id'))->first()->fill($request->all())->save();
            return redirect(route('admin.users.list'))->with('success', 'Пользователь сохранен');
        } else {
            return redirect(route('admin.users.list'))->with('error', 'Пользователь не сохранен');
        }
    }

    //Удаление пользователя
    public function delete($id, Request $request)
    {
        if ($id) {
            $Item = User::where('id', $id)->first();
            $Item->delete();
            return redirect(route('admin.users.list'))->with('success', 'Пользователь удален');
        } else {
            return redirect(route('admin.users.list'))->with('error', 'Пользователь не удален');
        }
    }

}
