<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\CategoryEditRequest;
use App\Models\Category;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CategoriesController extends Controller
{
    //Список категорий
    public function list(): View
    {
        $listItems = Category::GetCategoryTree();

        return view('backend/category/list', ['listItems' => $listItems]);
    }

    //Форма категорий
    public function edit($id): View
    {
        $categoryList = Category::GetAllCategories(0);

        $item = Category::where('id', $id)->first();

        return view('backend/category/edit', ['item' => $item, 'categoryList' => $categoryList]);
    }

    //Удаление категории
    public function delete($id): RedirectResponse
    {
        $item = Category::where('id', $id)->first();
        $item->delete();

        return redirect(route('admin.category.list'))->with('success', "Категория удалена");
    }

    //Сохранение категории
    public function save(CategoryEditRequest $request)
    {
        if ($request->input('item_id')) {
            Category::where('id', $request->input('item_id'))
                ->first()
                ->fill($request->all())
                ->save();
            $message = "Категория сохранена";
        } else {
            Category::create($request->all());
            $message = "Категория добавлена";
        }

        return redirect(route('admin.category.list'))->with('success', $message);
    }

}
