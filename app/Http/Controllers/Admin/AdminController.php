<?php

namespace App\Http\Controllers\Admin;

use App\Contracts\Services\CategoriesServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Helpers\Url;
use App\Http\Requests\Admin\User\UserEditRequest;
use App\Http\Requests\Category\CategoryEditRequest;
use App\Models\Category;
use App\Models\Region;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

ini_set('max_execution_time', 180000);
ini_set("pcre.backtrack_limit", "99999999999");
set_time_limit(0);

class AdminController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        return view('backend/index');
    }

    //Список контактов
    public function listContacts(Request $request)
    {
        $Contacts = DB::table('contacts')->where('type', 1)->orderBy('id', 'DESC')->paginate(50);
        return view('backend/contacts', ['listItems' => $Contacts]);

    }

    public function listAdvertsContacts(Request $request)
    {
        $Contacts = DB::table('contacts')->where('type', 2)->orderBy('id', 'DESC')->paginate(1);
        return view('backend/contacts-adverts', ['listItems' => $Contacts]);

    }
}
