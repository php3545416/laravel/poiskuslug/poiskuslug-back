<?php

namespace App\Http\Controllers\Profile;

use App\DTO\PageDataDTO;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\LinkAdvertsRequest;
use App\Http\Requests\User\LinkSubCategories;
use App\Models\Advert;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(
            function ($request, $next) {
                if ($request->is('logout')) {
                    return $next($request);
                }

                if (!empty(Auth::user()->user_type) && Auth::user()->user_type == 2) {
                    return redirect(route('admin.index'));
                }
                return $next($request);
            }
        );
    }

    public function index(): View
    {
        $dataPageDefault = (new PageDataDTO())
            ->setTitle("Услуги и цены");

        $categories = Category::GetAllCategories();

        $id = Auth::user()->id;

        $userCategories =  DB::table('category_user')
                ->where('user_id', $id)
                ->get()
                ->keyBy('sub_category_id')
                ->map(function ($category) {
                    $category->fields = $subCategory = Category::where('id', $category->sub_category_id)->first();
                    $category->children = $subCategory->adverts->where('can_be_selected', true);
                    return $category;
                });

        $userAdverts = DB::table('advert_user')
            ->where('user_id', $id)
            ->get()
            ->keyBy('advert_id')
            ->toArray();

        return view('user.profile.adverts.adverts', [
            'dataPageDefault' => $dataPageDefault,
            'allCategories' => $categories,
            'userCategories' => $userCategories,
            'userAdverts' => $userAdverts,
            'userCategoriesCount' => count($userCategories)
        ]);
    }

    public function addSubCategories(LinkSubCategories $request): JsonResponse
    {
        $selectedCategories = $request->validated('categories');

        $id = Auth::user()->id;

        DB::table('category_user')->where('user_id', $id)->delete();

        if ($selectedCategories) {
            $insertData = [];
            $categoriesToBySync = [];
            foreach ($selectedCategories as $key => $categories) {
                foreach ($categories as $category) {
                    $insertData[] = [
                        'user_id' => $id,
                        'category_id' => $key,
                        'sub_category_id' => $category,
                    ];
                    $categoriesToBySync[] = $category;
                }
            }

            $advertsToBySync = Advert::query()->whereHas('users', function ($query) use ($id) {
                $query->where('user_id', $id);
            })->whereIn('category_id', $categoriesToBySync)->pluck('id');

            User::query()->find($id)->adverts()->sync($advertsToBySync);

            DB::table('category_user')->insert($insertData);
        } else {
            User::query()->find($id)->adverts()->detach();
        }

        $userCategories =  DB::table('category_user')
            ->where('user_id', $id)
            ->get()
            ->keyBy('sub_category_id')
            ->map(function ($category) {
                $category->fields = $subCategory = Category::where('id', $category->sub_category_id)->first();
                $category->children = $subCategory->adverts->where('can_be_selected', true);
                return $category;
            });

        $userAdverts = DB::table('advert_user')
            ->where('user_id', $id)
            ->get()
            ->keyBy('advert_id')
            ->toArray();

        return response()->json([
            'success' => true,
            'alert' => view('notifications.success', [
                'Title' => "Услуги и цены ",
                'Text' => "Список категорий успешно сохранен"
            ])->render(),
            'prices' => view('user.profile.adverts.prices', [
                'userAdverts' => $userAdverts,
                'subCategories' => $userCategories,
            ])->render(),
            'userCategoriesCount' => count($userCategories)
        ]);
    }

    public function addAdvertPrice(LinkAdvertsRequest $request): JsonResponse
    {
        $selectedAdvertsWithPrices = $request->validated();
        $id = Auth::user()->id;

        DB::table('advert_user')->where('user_id', $id)->delete();

        if ($selectedAdvertsWithPrices) {
            $insertData = [];
            foreach ($selectedAdvertsWithPrices['adverts_price'] as $advert => $price) {
                $insertData[] = [
                    'user_id' => $id,
                    'advert_id' => $advert,
                    'price' => $price,
                ];
            }
            DB::table('advert_user')->insert($insertData);
        }

        $userCategories =  DB::table('category_user')
            ->where('user_id', $id)
            ->get()
            ->keyBy('sub_category_id')
            ->map(function ($category) {
                $category->fields = $subCategory = Category::where('id', $category->sub_category_id)->first();
                $category->children = $subCategory->adverts->where('can_be_selected', true);
                return $category;
            });

        $userAdverts = DB::table('advert_user')
            ->where('user_id', $id)
            ->get()
            ->keyBy('advert_id')
            ->toArray();

        return response()->json([
            'success' => true,
            'alert' => view('notifications.success', [
                'Title' => "Услуги и цены",
                'Text' => "Список услуг успешно сохранен",
            ])->render(),
            'prices' => view('user.profile.adverts.prices', [
                'userAdverts' => $userAdverts,
                'subCategories' => $userCategories,
            ])->render(),
            'advertsCount' => count($userAdverts)
        ]);
    }

    public function setSessionProfileParam(Request $request): void
    {
        $request->session()->put('showConfirmationModal', false);
    }

}
