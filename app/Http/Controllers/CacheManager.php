<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
ini_set('max_execution_time', 10000);

class CacheManager extends Controller
{
    public function advertsCountManager(Request $request)
    {
        $start = microtime(TRUE);
        $Regions = $this->getRegionsWithAdverts();
        $Citys = $this->getCitysWithAdverts();

        //Создаем кэш для регионов
        foreach ($Regions as $Region) {
            $this->setAdvertCountCache($Region->id, 0, 0, 0, !empty($Region->adverts_count) ? $Region->adverts_count : 0);
        }

        //Проходим по городам, где есть активные объявления
        foreach ($Citys as $City) {
            $this->setAdvertCountCache($City->id, 0, 0, 0, $City->adverts_count);

            //Получаем категории с объявлениями для города
            $Categories = $this->getCategoriesWithAdverts($City->id);
            if ($Categories) {
                foreach ($Categories as $Category) {
                    $this->setAdvertCountCache(0, $City->id, $Category->id, 0, $Category->adverts_count);
                }
            }
            $Categories = $this->getCategoriesWithAdvertsDistinct($City->id);
            if ($Categories) {
                foreach ($Categories as $Category) {
                    $this->setAdvertCountCacheDistinct(0, $City->id, $Category->id, 0, $Category->adverts_count);
                }
            }

            //Получаем подкатегории с объявлениями для города
            $Categories = $this->getSubCategoriesWithAdverts($City->id);
            if ($Categories) {
                foreach ($Categories as $Category) {
                    $this->setAdvertCountCache(
                        0, $City->id, $Category->parent_id, $Category->id, $Category->adverts_count
                    );
                }
            }
            //Получаем подкатегории с объявлениями для города
            $Categories = $this->getSubCategoriesWithAdvertsDistinct($City->id);
            if ($Categories) {
                foreach ($Categories as $Category) {
                    $this->setAdvertCountCacheDistinct(
                        0, $City->id, $Category->parent_id, $Category->id, $Category->adverts_count
                    );
                }
            }
        }

        echo '<b>' . (microtime(TRUE) - $start) . '</b>';
    }

    private function setAdvertCountCache($RegionID = 0, $CityID = 0, $CategoryID = 0, $SubCategoryID = 0, $Count = 0)
    {
        if ($Count > 0 || Cache::has("advert_count_cache_{$RegionID}_{$CityID}_{$CategoryID}_{$SubCategoryID}")) {
            Cache::put("advert_count_cache_{$RegionID}_{$CityID}_{$CategoryID}_{$SubCategoryID}", $Count, 3600 * 24);
        }
    }

    private function setAdvertCountCacheDistinct(
        $RegionID = 0, $CityID = 0, $CategoryID = 0, $SubCategoryID = 0, $Count = 0
    ) {
        if ($Count > 0 || Cache::has("advert_count_distinct_cache_{$RegionID}_{$CityID}_{$CategoryID}_{$SubCategoryID}")) {
            Cache::put(
                "advert_count_distinct_cache_{$RegionID}_{$CityID}_{$CategoryID}_{$SubCategoryID}", $Count, 3600 * 24
            );
        }
    }
}
