<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\CategoriesRepositoryContract;
use App\Contracts\Services\SeoPagesServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\DTO\UsersFilterDTO;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(
        private readonly CategoriesRepositoryContract $categoriesRepository,
        private readonly SeoPagesServiceContract $seoPagesService
    ) {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $usersFilterDTO = (new UsersFilterDTO())
            ->setLimit(20)
            ->setRegionId($request->session()->get('city'))
            ->setRelations(['parent']);

        $categories = $this->categoriesRepository->getHomeCategories($usersFilterDTO);

        $dataPageDefault = $this->seoPagesService->getSeoPageData(
            'home',
            $request->session()->get('city')
        );

        return view('home', [
            'categories' => $categories,
            'dataPageDefault'=> $dataPageDefault
        ]);
    }

    public function contacts(Request $request)
    {
        $dataPageDefault = $this->seoPagesService->getSeoPageData(
            'support',
            $request->session()->get('region')
        );

        $advert_id = $request->input('advert_id');

        return view('contacts', [
            'dataPageDefault' => $dataPageDefault,
            'advert_id' => $advert_id]
        );
    }

    public function userterms(Request $request)
    {
        $dataPageDefault = $this->seoPagesService->getSeoPageData(
            'user_terms',
            $request->session()->get('region')
        );

        return view('userterms', ['dataPageDefault' => $dataPageDefault]);
    }

    public function news(Request $request)
    {
        $dataPageDefault = $this->seoPagesService->getSeoPageData('news_page');
        return view('userterms', ['dataPageDefault' => $dataPageDefault]);
    }

    public function userpolitics(Request $request)
    {
        $dataPageDefault = $this->seoPagesService->getSeoPageData(
            'user_politics',
            $request->session()->get('region')
        );
        return view('userterms', ['dataPageDefault' => $dataPageDefault]);
    }

    public function contactsSend(Request $request)
    {
        if ($request->input('email') != '') {
            return response()->json(['errors' => array()], 400);
        }
        //Отправка со страницы объявления
        if (!empty($request->input('field')['adv_id_current'])) {
            $Errors = $this->ValidationAdvertContactsForm($request->input('field'));
            if ($Errors->fails()) {
                return response()->json(['errors' => $Errors->errors()->toArray()], 400);
            } else {
                DB::table('contacts')->insert(
                    [
                        'adv_id' => $request->input('field')['adv_id_current'], 'name' => '',
                        'email'  => $request->input('field')['contact'], 'theme' => '',
                        'text'   => $request->input('field')['text'], 'date_add' => date('Y-m-d H:i:s'), 'type' => 2
                    ]
                );
                return response()->json(['success' => 1,'text'=>'Ваше сообщение отправленно. По возможности, мастер свяжется с вами.']);
                /*$mData = array();
                $mData['inputs'] = $request->input('field');
                Mail::send(
                    'mail.contacts', ["mData" => $mData], function ($message) use ($mData) {
                    $message->to(env('APP_ADMIN_EMAIL'));
                    $message->subject('Служба поддержки');
                }
                );*/
            }
        } else {
            //Отправка со страницы контактов
            $Errors = $this->ValidationContactsForm($request->input('field'));
            if ($Errors->fails()) {
                return response()->json(['errors' => $Errors->errors()->toArray()], 400);
            } else {
                DB::table('contacts')->insert(
                    [
                        'adv_id' => $request->input('field')['adv_id'], 'name' => $request->input('field')['name'],
                        'email'  => $request->input('field')['email'], 'theme' => $request->input('field')['theme'],
                        'text'   => $request->input('field')['text'], 'date_add' => date('Y-m-d H:i:s'), 'type' => 1
                    ]
                );
                $mData = array();
                $mData['inputs'] = $request->input('field');
                Mail::send(
                    'mail.contacts', ["mData" => $mData], function ($message) use ($mData) {
                    $message->to(env('APP_ADMIN_EMAIL'));
                    $message->subject('Служба поддержки');
                }
                );
            }

        }

        return response()->json(['success' => 1,'text'=>'Ваша заявка успешно отправлена!']);
    }

    //Валидация формы контактов
    private function validationContactsForm($data)
    {
        return Validator::make(
            $data, [
            'name'  => 'required|string|max:190',
            'text'  => 'required|string|max:10000',
            'email' => 'required|string|email|max:190',
        ], [

                'name.required'  => 'Укажите имя',
                'text.required'  => 'Введите сообщение',
                'email.required' => 'Укажите свой E-mail',
                'email.email'    => 'Проверьте E-mail',

            ]
        );
    }
    //Валидация формы контактов в объявлении
    private function validationAdvertContactsForm($data)
    {
        return Validator::make(
            $data, [
            'contact'  => 'required|string|max:190',
            'text'  => 'required|string|max:10000',
        ], [

                'contact.required'  => 'Укажите контакты',
                'text.required'  => 'Введите сообщение',

            ]
        );
    }

}
