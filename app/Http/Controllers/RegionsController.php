<?php

namespace App\Http\Controllers;

use App\Contracts\Repositories\CategoriesRepositoryContract;
use App\Contracts\Repositories\RegionsRepositoryContract;
use App\Contracts\Services\RegionsServiceContract;
use App\Contracts\Services\SeoPagesServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\DTO\UsersFilterDTO;
use Illuminate\Http\Request;
use Illuminate\View\View;

class RegionsController extends Controller
{
    public function __construct(
        private readonly SeoPagesServiceContract $seoPagesService,
        private readonly RegionsServiceContract $regionsService,
        private readonly RegionsRepositoryContract $regionsRepository,
        private readonly CategoriesRepositoryContract $categoriesRepository
    ) {
    }

    public function index(Request $request): View
    {
        $usersFilterDTO = (new UsersFilterDTO())
            ->setRegionId($request->session()->get('city'))
            ->setRelations(['parent']);

        $categories = $this->categoriesRepository->getHomeRegionsCategories($usersFilterDTO);

        $dataPageDefault = $this->seoPagesService->getSeoPageData(
            'home_region',
            $request->session()->get('city')
        );

        return view('home', [
            'categories' => $categories,
            'dataPageDefault'=> $dataPageDefault
        ]);
    }

    public function showRegionList(): View
    {
        $regions = $this->regionsService->getAllRegions();
        $dataPageDefault = $this->seoPagesService->getSeoPageDataForRegionsList('select_region');

        $breadsRegion = [
            route('region.index') => 'Россия',
        ];

        return view('regions.list',[
            'breadsRegion' => $breadsRegion,
            'regions' => $regions,
            'dataPageDefault' => $dataPageDefault
        ]);
    }

    public function showRegionCityList(int $regionId): View
    {
        $regions = $this->regionsService->getRegionCities($regionId, 10);

        $region = $this->regionsRepository->getRegionById($regionId);

        $dataPageDefault = $this->seoPagesService->getSeoPageData('select_city', $regionId);

        $breadsRegion = [
            route('region.index') => 'Россия',
            '0' => $region->name
        ];

        return view('regions.list', [
            'regions' => $regions,
            'breadsRegion' => $breadsRegion,
            'dataPageDefault' => $dataPageDefault
        ]);
    }
}
