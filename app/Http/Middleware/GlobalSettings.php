<?php

namespace App\Http\Middleware;

use App\Helpers\Helpers;
use App\Models\Advert;
use App\Models\Region;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class GlobalSettings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->route('region')){
            $CityRegion = Region::GetRegionByURL($request->route('region'));
            $CityRegionDefault = $CityRegion;
        }

        if(!isset($CityRegion) || !$CityRegion){
            if(!empty($request->session()->get('city_url'))&&$request->session()->get('city_url')!=''){
                $CityRegionDefault = Region::GetRegionByURL($request->session()->get('city_url'));
                $CityRegion = $CityRegionDefault;
            }else{
                $CityRegionDefault = Region::GetRegionByURL('moskva');
                $CityRegion = $CityRegionDefault;
            }

        }

        $Banners = DB::table('banners')->get();

        $HeaderNotification = DB::table('seo_settings')->where('page_type','header_text')->first();

        $request->session()->put('city', $CityRegion->id);
        $request->session()->put('region', $CityRegionDefault->id);
        view()->share('measureList', Helpers::measureList());
        view()->share('CityRegion', $CityRegion);
        view()->share('CityRegionDefault', $CityRegionDefault);
        view()->share('BannersPages', $Banners);
        view()->share('HeaderNotification', $HeaderNotification);
        view()->share('Version', time());
        return $next($request);
    }
}
