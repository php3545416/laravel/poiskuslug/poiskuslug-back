<?php

namespace App\Http\Middleware;

use App\Contracts\Services\RegionsServiceContract;
use Closure;

class RegionsMiddleware
{
    public function __construct(private readonly RegionsServiceContract $regionsService)
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $regionRouteName = $request->route('region');
        $city = $this->regionsService->getCity($regionRouteName);

        $request->session()->put('city', $city->id);
        $request->session()->put('city_url', $city->url);
        $request->session()->save();

        return $next($request);
    }
}
