<?php

namespace App\Services;

use App\Contracts\Repositories\UsersRepositoryContract;
use App\Contracts\Services\UserImagesServiceContract;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Intervention\Image\Facades\Image as IntImage;
use App\Models\Image;

class UserImagesService implements UserImagesServiceContract
{
    public function __construct(private readonly UsersRepositoryContract $usersRepository)
    {
    }
    public function linkPortfolioImageToUser(int $userId, UploadedFile $file): Image
    {
        $filePath = public_path('storage');
        $fileName = "/users/{$userId}/"  . md5(time() . '_' . rand(100, 200));

        if (! file_exists($filePath . "/users/{$userId}/")) {
            mkdir($filePath . "/users/{$userId}/", 0777, true);
        }

        $img = IntImage::make($file->path());

        $img->resize(
            700, 700, function ($const) {
            $const->aspectRatio();
        })
            ->save($filePath . $fileName . '_big.jpg', 80, 'jpg');

        $img->resize(
            150, 150, function ($const) {
            $const->aspectRatio();
        })
            ->save($filePath . $fileName . '_small.jpg', 80, 'jpg');

        return Image::create([
            'user_id' => $userId,
            'path'    => $fileName . '_small.jpg'
        ]);
    }

    public function addProfilePhoto(int $userId, UploadedFile $file): User
    {
        $filePath = public_path('storage');
        $fileName = "/users/{$userId}/"  . md5(time() . '_' . rand(100, 200));

        if (! file_exists($filePath . "/users/{$userId}/")) {
            mkdir($filePath . "/users/{$userId}/", 0777, true);
        }

        $img = IntImage::make(file_get_contents($file));

        $img->resize(
            300, 300, function ($const) {
            $const->aspectRatio();
        })
            ->save($filePath . $fileName . '.jpg', 80, 'jpg');


        $user = $this->usersRepository->getUserById($userId);

        if ($user->photo) {
            @unlink($filePath . $user->photo);
        }

        $user->photo = $fileName . '.jpg';
        $user->save();

        return $user;
    }
}
