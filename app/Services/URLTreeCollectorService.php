<?php

namespace App\Services;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Contracts\Repositories\CategoriesRepositoryContract;
use App\Contracts\Services\RegionsServiceContract;
use App\Contracts\Services\URLTreeCollectorServiceContract;
use App\DTO\URLTreeDTO;

class URLTreeCollectorService implements URLTreeCollectorServiceContract
{
    public function __construct(
        private readonly CategoriesRepositoryContract $categoriesRepository,
        private readonly AdvertsRepositoryContract $advertsRepository,
        private readonly RegionsServiceContract $regionsService
    ) {
    }

    public function collectURLTree(
        string $regionCode,
        string $categoryCode,
        string $subCategoryCode = null,
        string $advertCode = null
    ): URLTreeDTO {
        $region = $this->regionsService->getCity($regionCode);
        $category = $this->categoriesRepository->getCategoryByCode($categoryCode);
        $subCategory = $subCategoryCode ? $this->categoriesRepository->getCategoryByCode($subCategoryCode) : $subCategoryCode;
        $advert = $advertCode ? $this->advertsRepository->getAdvertByCode($advertCode) : $advertCode;

        return (new URLTreeDTO())
            ->setRegion($region)
            ->setCategory($category)
            ->setSubCategory($subCategory)
            ->setAdvert($advert);
    }
}
