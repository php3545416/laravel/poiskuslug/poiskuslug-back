<?php

namespace App\Services;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Contracts\Services\AdvertCreateServiceContract;
use App\Contracts\Services\AdvertRemoveServiceContract;
use App\Contracts\Services\AdvertsServiceContract;
use App\Contracts\Services\AdvertUpdateServiceContract;
use App\Models\Advert;

class AdvertsService implements AdvertCreateServiceContract, AdvertUpdateServiceContract, AdvertRemoveServiceContract
{
    public function __construct(
        private readonly AdvertsRepositoryContract $advertsRepository
    ) {
    }

    public function update(int $id, array $fields): Advert
    {
        return $this->advertsRepository->update($id, $fields);
    }

    public function delete(int $id): int
    {
        return $this->advertsRepository->delete($id);
    }

    public function create(array $fields): Advert
    {
        return $this->advertsRepository->create($fields);
    }
}
