<?php

namespace App\Services;

use App\Contracts\Repositories\UsersRepositoryContract;
use App\Contracts\Services\UpdateUserProfileServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\DTO\UsersFilterDTO;
use App\Helpers\Helpers;
use App\Http\Helpers\Url;
use App\Models\Advert;
use App\Models\Category;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class UsersService implements UsersServiceContract, UpdateUserProfileServiceContract
{
    public function __construct(
        private readonly UsersRepositoryContract $usersRepository
    ) {
    }

    public function getUsersByRegion(UsersFilterDTO $usersFilterDTO): Collection|LengthAwarePaginator
    {
        return $this->prepareUsersPhone($this->usersRepository->getUsersForHomePage($usersFilterDTO));
    }

    public function getUsersByRegionAndAdvert(UsersFilterDTO $usersFilterDTO, ?Advert $advert = null): LengthAwarePaginator
    {
        $users = $this->prepareUsersPhone($this->usersRepository->getUsersByRegionAndAdvert($usersFilterDTO));

        $users->totalString = $this->getUsersTotalString($users, $advert);

        return $users;
    }

    public function prepareUsersPhone(Collection|LengthAwarePaginator|User $collection): Collection|User|LengthAwarePaginator
    {
        if ($collection instanceof Collection || $collection instanceof LengthAwarePaginator) {
            $collection->map(function ($item) {
                $item->phone = Url::PhoneHidden($item->phone);
                return $item;
            });
        } else {
            $collection->phone = Url::PhoneHidden($collection->phone);
        }

        return $collection;
    }

    public function getUsersByRegionAndCategory(UsersFilterDTO $usersFilterDTO, ?Category $category = null): LengthAwarePaginator
    {
        $users = $this->prepareUsersPhone($this->usersRepository->getUsersByRegionAndCategory($usersFilterDTO));

        $users->totalString = $this->getUsersTotalString($users, $category);

        return $users;
    }

    public function getUsersByRegionAndSubCategory(UsersFilterDTO $usersFilterDTO, ?Category $category = null): LengthAwarePaginator
    {
        $users = $this->prepareUsersPhone($this->usersRepository->getUsersByRegionAndSubCategory($usersFilterDTO));

        $users->totalString = $this->getUsersTotalString($users, $category);

        return $users;
    }

    public function getUserData(int $id): User
    {
        return $this->prepareUsersPhone($this->usersRepository->getUserData($id, ['images', 'categories', 'adverts']));
    }

    public function upProfile(int $userId): User
    {
        return $this->usersRepository->update($userId, ['last_profile_up_at' => Carbon::now()]);
    }

    private function getUsersTotalString(LengthAwarePaginator $users, Category|Advert $category): string
    {
        return $users->total() . ' ' . Helpers::wordWithCorrectEnding(
            $users->total(),
            [
                $category->master_1,
                $category->master_2,
                $category->master_3
            ])
        ;
    }

    public function getUserByPhone(UsersFilterDTO $usersFilterDTO): ?User
    {
        return $this->usersRepository->getUserByPhone(
            $usersFilterDTO->getPhone()
        );
    }
}
