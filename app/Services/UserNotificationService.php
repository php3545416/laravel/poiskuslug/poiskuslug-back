<?php

namespace App\Services;

use App\Contracts\Services\UserNotificationServiceContract;
use App\Models\User;
use App\Notifications\EmailVerificationNotification;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class UserNotificationService implements UserNotificationServiceContract
{

    public function sendVerificationMail(Authenticatable $user, ?string $newEmail = null): bool
    {
        try {
            if ($newEmail) {
                $user->email = $newEmail;
                $user->email_verified_at = null;
            }

            Notification::send($user, new EmailVerificationNotification($user));

            $user->save();
            return true;
        } catch (\Exception $exception) {
            Log::debug('Не удалось отправить подтверждение на почту: '.$user->email, [
                'message' => $exception->getMessage(),
            ]);

            return false;
        }
    }
}