<?php

namespace App\Services;

use App\Contracts\Repositories\CategoriesRepositoryContract;
use App\Contracts\Services\CategoriesServiceContract;
use App\DTO\CategoriesFilterDTO;
use App\Models\Category;
use Illuminate\Support\Collection;

class CategoriesService implements CategoriesServiceContract
{
    public function __construct(
        private readonly CategoriesRepositoryContract $categoriesRepository
    ) {
    }

    public function getCategoryUrlInfoRegion(string $code): Category
    {
        $category = $this->categoriesRepository->getCategoryUrlInfoRegion($code);

        if(! empty($CheckCategory)) {
            $category->adverts_count = 0;
        }

        return $category;
    }

    public function getSubCategories(CategoriesFilterDTO $categoriesFilter): Collection
    {
        return $this->categoriesRepository->getSubCategories($categoriesFilter);
    }

    public function getCategoriesTree(): Collection
    {
    }

}
