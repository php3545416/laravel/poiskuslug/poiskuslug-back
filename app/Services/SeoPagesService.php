<?php

namespace App\Services;

use App\Contracts\Repositories\RegionsRepositoryContract;
use App\Contracts\Repositories\SeoPagesRepositoryContract;
use App\Contracts\Services\SeoPagesServiceContract;
use App\Enums\SeoVariables;
use App\Helpers\Helpers;
use App\Models\Advert;
use App\Models\Category;
use App\Models\Region;
use App\Models\SeoPage;
use Illuminate\Support\Collection;

class SeoPagesService implements SeoPagesServiceContract
{
    public function __construct(
        private readonly RegionsRepositoryContract $regionsRepository,
        private readonly SeoPagesRepositoryContract $seoPagesRepository
    ) {
    }

    public function getSeoPageData(string $pageType, int $regionId = null): ?SeoPage
    {
        $seoPage = $this->seoPagesRepository->getSeoPageData($pageType);

        $region = $regionId !== null ? $this->regionsRepository->getRegionById($regionId) : null;

        return $region !== null ? $this->fillDataSeoPage($seoPage, $region) : null;
    }

    public function getSeoPageDataForRegionsList(string $pageType, int $regionId = null): ?SeoPage
    {
        return $this->seoPagesRepository->getSeoPageData($pageType);
    }

    public function fillDataSeoPage(
        SeoPage $seoPage,
        Region $region,
        ?int $mastersCount = null,
        Category|Advert|null $entity = null
    ): SeoPage {
        return $this->replace($seoPage, $region, ['title', 'description', 'text', 'h1'], $mastersCount, $entity);
    }

    private function replace(
        SeoPage $seoPage,
        Region $city,
        array $fields,
        ?int $mastersCount = null,
        Category|Advert|null $entity = null
    ): SeoPage
    {
        if ($city->parent_id === null || $city->parent_id == 0) {
            $region = $city;
        } else {
            $region = Region::query()->find($city->parent_id);
        }

        foreach ($fields as $field) {
            $seoPage->{$field} = str_replace([
                "{region.in}",
                "{city.in}",
                "{region}",
                "{city}",
                "{total.m}",
                "{master.text}"
            ], [
                $region->name_case,
                $city->name_case,
                $region->name,
                $city->name,
                $mastersCount,
                $entity ? Helpers::wordWithCorrectEnding($mastersCount, [$entity->master_1, $entity->master_2, $entity->master_3]) : null
            ], $seoPage->{$field});
        }

        return $seoPage;
    }
}
