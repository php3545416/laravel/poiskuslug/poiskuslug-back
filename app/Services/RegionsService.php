<?php

namespace App\Services;

use App\Contracts\Repositories\RegionsRepositoryContract;
use App\Contracts\Services\RegionsServiceContract;
use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class RegionsService implements RegionsServiceContract
{
    public function __construct(private readonly RegionsRepositoryContract $regionsRepository)
    {
    }

    public function getRegionFromRequest(
        Request $request,
        string $sessionParam
    ): string {
        $cityRegion = $request->route('region') ?
            $this->regionsRepository->getRegionByURL($request->route('region')) : null;

        if ($cityRegion === null) {
            if (! empty($request->session()->get($sessionParam))) {
                $cityRegion = $this->regionsRepository->getRegionByURL($request->session()->get($sessionParam));
            } else {
                $cityRegion = $this->regionsRepository->getRegionByURL(config('app.region.default'));
            }
        }

        return $cityRegion;
    }

    public function getCity(string $code): Region
    {
        return $this->regionsRepository->getCity($code);
    }

    public function getAllRegions(int $chunkCount = 350): Collection
    {
        return $this->regionsRepository->getAllRegions($chunkCount);
    }

    public function getRegionCities(int $regionId, int $chunkCount = 10): Collection
    {
        return $this->regionsRepository->getRegionCities($regionId, $chunkCount);
    }
}
