<?php

namespace App\Notifications;

use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;

class EmailVerificationNotification extends Notification
{
    /**
     * Create a new notification instance.
     */
    public function __construct(
        public Authenticatable $user,
    ) {}

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(): MailMessage
    {
        return $this->buildMailMessage($this->makeVerificationUrl());
    }

    protected function buildMailMessage(string $verificationUrl): MailMessage
    {
        return (new MailMessage)
            ->subject('📩 Подтвердите вашу электронную почту - Поиск Услуг')
            ->markdown('emails.email-verification', [
                'user' => $this->user,
                'url' => $verificationUrl
            ]);
    }

    protected function makeVerificationUrl(): string
    {
        return URL::temporarySignedRoute(
            'email-verification.verify',
            Carbon::now()->addDay(),
            [
                'id' => $this->user->getKey(),
                'hash' => sha1($this->user->getEmailForVerification()),
            ]
        );
    }
}
