<?php

namespace App\Providers;

use App\Contracts\Services\AdvertCreateServiceContract;
use App\Contracts\Services\AdvertRemoveServiceContract;
use App\Contracts\Services\AdvertUpdateServiceContract;
use App\Contracts\Services\CategoriesServiceContract;
use App\Contracts\Services\ImagesServiceContract;
use App\Contracts\Services\RegionsServiceContract;
use App\Contracts\Services\SeoPagesServiceContract;
use App\Contracts\Services\UpdateUserProfileServiceContract;
use App\Contracts\Services\URLTreeCollectorServiceContract;
use App\Contracts\Services\UserImagesServiceContract;
use App\Contracts\Services\UserNotificationServiceContract;
use App\Contracts\Services\UsersServiceContract;
use App\Models\Image;
use App\Services\AdvertsService;
use App\Services\CategoriesService;
use App\Services\ImagesService;
use App\Services\RegionsService;
use App\Services\SeoPagesService;
use App\Services\URLTreeCollectorService;
use App\Services\UserImagesService;
use App\Services\UserNotificationService;
use App\Services\UsersService;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            AdvertCreateServiceContract::class,
            AdvertsService::class
        );

        $this->app->singleton(
            AdvertUpdateServiceContract::class,
            AdvertsService::class
        );

        $this->app->singleton(
            AdvertRemoveServiceContract::class,
            AdvertsService::class
        );

        $this->app->singleton(
            CategoriesServiceContract::class,
            CategoriesService::class
        );

        $this->app->singleton(
            RegionsServiceContract::class,
            RegionsService::class
        );

        $this->app->singleton(
            UsersServiceContract::class,
            UsersService::class
        );

        $this->app->singleton(
            UpdateUserProfileServiceContract::class,
            UsersService::class
        );

        $this->app->singleton(
            SeoPagesServiceContract::class,
            SeoPagesService::class
        );

        $this->app->singleton(
            URLTreeCollectorServiceContract::class,
            URLTreeCollectorService::class
        );

        $this->app->singleton(ImagesServiceContract::class, function () {
            return $this->app->make(ImagesService::class, ['disk' => 'public']);
        });

        $this->app->singleton(
            UserImagesServiceContract::class,
            UserImagesService::class
        );

        $this->app->singleton(
            UserNotificationServiceContract::class,
            UserNotificationService::class
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Валидация количества фотографий для пользователя
        Validator::extend('userphotos', function($attribute, $value, $parameters) {
            $photos = Image::where('user_id',Auth::user()->id)->count();
            return $photos  < 20;
        });

        //Прослушивание всех запросов к БД
       /* DB::listen(function($query) {

            $query1 = str_replace(array('?'), array('\'%s\''), $query->sql);
            $query1 = vsprintf($query1,$query->bindings);
            //dump($query);

            Log::info(
                $query1."!!!". $query->time/1000
            );
        });*/
    }
}
