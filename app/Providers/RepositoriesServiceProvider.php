<?php

namespace App\Providers;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Contracts\Repositories\CategoriesRepositoryContract;
use App\Contracts\Repositories\ImagesRepositoryContract;
use App\Contracts\Repositories\RegionsRepositoryContract;
use App\Contracts\Repositories\SeoPagesRepositoryContract;
use App\Contracts\Repositories\UsersRepositoryContract;
use App\Repositories\AdvertsRepository;
use App\Repositories\CategoriesRepository;
use App\Repositories\ImagesRepository;
use App\Repositories\RegionsRepository;
use App\Repositories\SeoPagesRepository;
use App\Repositories\UsersRepository;
use Illuminate\Support\ServiceProvider;

class RepositoriesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            AdvertsRepositoryContract::class,
            AdvertsRepository::class
        );

        $this->app->singleton(
            UsersRepositoryContract::class,
            UsersRepository::class
        );

        $this->app->singleton(
            RegionsRepositoryContract::class,
            RegionsRepository::class
        );

        $this->app->singleton(
            CategoriesRepositoryContract::class,
            CategoriesRepository::class
        );

        $this->app->singleton(
            SeoPagesRepositoryContract::class,
            SeoPagesRepository::class
        );

        $this->app->singleton(
            ImagesRepositoryContract::class,
            ImagesRepository::class
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
}
