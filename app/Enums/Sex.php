<?php

namespace App\Enums;

enum Sex: string
{
    case M = 'M';
    case W = 'W';

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }
}
