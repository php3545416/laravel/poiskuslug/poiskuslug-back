<?php

namespace App\Enums;

enum SeoVariables: string
{
    case REGION_IN = "{region.in}";
    case CITY_IN = "{regio.in}";
    case CITY = "{city}";
    case REGION = "{region}";
    case MASTER_TEXT = "{master.text}";
    case TOTAL_M = "{total.m}";
}
