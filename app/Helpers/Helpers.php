<?php

namespace App\Helpers;

class Helpers
{
    public static function measureList(): array
    {
        return [
            1  => 'за услугу',
            2  => 'за урок',
            3  => 'за 1 м²',
            4  => 'за 1 м³',
            5  => 'за 1 пог. метр',
            6  => 'за 1 шт.',
            7  => 'за 1 метр',
            8  => 'за кг.',
            9  => 'за 1 км',
            10 => 'за 45 мин.',
            11 => 'за 1 час +',
            12 => 'за 1 сутки',
            13 => 'за 1 неделю',
            14 => 'за 1 месяц',
        ];
    }

    public static function wordWithCorrectEnding(int $number, array $titles): string|null
    {
        $cases = [2, 0, 1, 1, 1, 2];

        return $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }
}
