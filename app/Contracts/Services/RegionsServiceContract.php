<?php

namespace App\Contracts\Services;

use App\Models\Region;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

interface RegionsServiceContract
{
    public function getRegionFromRequest(
        Request $request,
        string $sessionParam
    ): string;

    public function getCity(string $code): Region;

    public function getAllRegions(int $chunkCount = 350): Collection;

    public function getRegionCities(int $regionId, int $chunkCount = 10): Collection;
}
