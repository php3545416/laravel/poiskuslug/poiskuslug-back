<?php

namespace App\Contracts\Services;

use App\DTO\UsersFilterDTO;
use App\Models\Advert;
use App\Models\Category;
use App\Models\Image;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface UsersServiceContract
{
    public function getUsersByRegion(UsersFilterDTO $usersFilterDTO): Collection|LengthAwarePaginator;

    public function prepareUsersPhone(Collection|User $collection): Collection|User|LengthAwarePaginator;

    public function getUsersByRegionAndAdvert(UsersFilterDTO $usersFilterDTO, ?Advert $advert = null): LengthAwarePaginator;

    public function getUsersByRegionAndCategory(UsersFilterDTO $usersFilterDTO, ?Category $category= null): LengthAwarePaginator;

    public function getUsersByRegionAndSubCategory(UsersFilterDTO $usersFilterDTO, ?Category $category = null): LengthAwarePaginator;

    public function getUserData(int $id): User;

    public function getUserByPhone(UsersFilterDTO $usersFilterDTO): ?User;
}
