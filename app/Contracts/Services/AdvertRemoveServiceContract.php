<?php

namespace App\Contracts\Services;

interface AdvertRemoveServiceContract
{
    public function delete(int $id): int;
}
