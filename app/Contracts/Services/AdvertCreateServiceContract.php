<?php

namespace App\Contracts\Services;

use App\Models\Advert;

interface AdvertCreateServiceContract
{
    public function create(array $fields): Advert;
}
