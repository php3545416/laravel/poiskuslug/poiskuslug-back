<?php

namespace App\Contracts\Services;

use Illuminate\Contracts\Auth\Authenticatable;

interface UserNotificationServiceContract
{
    public function sendVerificationMail(Authenticatable $user, ?string $newEmail = null): bool;
}