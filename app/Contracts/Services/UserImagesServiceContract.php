<?php

namespace App\Contracts\Services;

use App\Models\Image;
use App\Models\User;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;

interface UserImagesServiceContract
{
    public function linkPortfolioImageToUser(int $userId, UploadedFile $file): Image;

    public function addProfilePhoto(int $userId, UploadedFile $file): User;
}

