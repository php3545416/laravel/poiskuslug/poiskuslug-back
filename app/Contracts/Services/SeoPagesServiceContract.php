<?php

namespace App\Contracts\Services;

use App\Models\Advert;
use App\Models\Category;
use App\Models\Region;
use App\Models\SeoPage;
use Illuminate\Support\Collection;

interface SeoPagesServiceContract
{
    public function getSeoPageData(string $pageType, int $regionId = null): ?SeoPage;

    public function fillDataSeoPage(SeoPage $seoPage, Region $region, ?int $mastersCount = null, Category|Advert|null $entity = null): SeoPage;
}
