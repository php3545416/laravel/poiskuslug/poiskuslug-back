<?php

namespace App\Contracts\Services;

use App\Models\Advert;

interface AdvertUpdateServiceContract
{
    public function update(int $id, array $fields): Advert;
}
