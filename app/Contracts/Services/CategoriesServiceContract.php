<?php

namespace App\Contracts\Services;

use App\DTO\CategoriesFilterDTO;
use App\Models\Category;
use Illuminate\Support\Collection;

interface CategoriesServiceContract
{
    public function getCategoryUrlInfoRegion(string $code): Category;

    public function getSubCategories(CategoriesFilterDTO $categoriesFilter): Collection;

    public function getCategoriesTree(): Collection;
}
