<?php

namespace App\Contracts\Services;

use App\DTO\URLTreeDTO;

interface URLTreeCollectorServiceContract
{
    public function collectURLTree(
        string $regionCode,
        string $categoryCode,
        string $subCategoryCode = null,
        string $advertCode = null
    ): URLTreeDTO;
}
