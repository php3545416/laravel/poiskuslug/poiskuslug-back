<?php

namespace App\Contracts\Services;

use App\Models\User;

interface UpdateUserProfileServiceContract
{
    /**
     * @param int $userId
     * @return User
     */
    public function upProfile(int $userId): User;
}
