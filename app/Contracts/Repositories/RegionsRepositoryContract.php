<?php

namespace App\Contracts\Repositories;

use App\Models\Region;
use App\Models\User;
use Illuminate\Support\Collection;

interface RegionsRepositoryContract
{
    public function getRegionByURL(string $paramFromURL): Region;

    public function getCity(string $code): Region;

    public function getRegionByCode(string $code): Region;

    public function getRegionById(int $id): Region;

    public function getAllRegions(int $chunkCount = 350, bool $isOnlyPublic = true): Collection;

    public function getRegionCities(int $regionId, int $chunkCount = 10): Collection;
}
