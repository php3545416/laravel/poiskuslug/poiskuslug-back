<?php

namespace App\Contracts\Repositories;

use App\Models\Advert;
use Illuminate\Support\Collection;

interface AdvertsRepositoryContract
{
    public function getModel(): Advert;

    public function getAdvertByCode(string $code): Advert;

    public function getPopularAdvertsByCategory(int $categoryId, int $regionId): Collection;

    public function getSidePanelAdvertsByCategory(int $categoryId, int $regionId): Collection;

    public function getById(int $id): Advert;

    public function create(array $fields): Advert;

    public function update(int $id, array $fields): Advert;

    public function delete(int $id): int;
}
