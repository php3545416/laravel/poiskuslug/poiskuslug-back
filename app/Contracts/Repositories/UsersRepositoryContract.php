<?php

namespace App\Contracts\Repositories;

use App\DTO\UsersFilterDTO;
use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface UsersRepositoryContract
{
    /**
     * @param int $id
     * @return User
     */
    public function getUserById(int $id): User;

    /**
     * @param string $phone
     * @return User|null
     */
    public function getUserByPhone(string $phone): ?User;

    /**
     * @param int $userId
     * @param array $relations
     * @return Collection
     */
    public function getUserAdverts(int $userId, array $relations = []): Collection;

    /**
     * @param int $userId
     * @param array $relations
     * @return Collection
     */
    public function getUserCategories(int $userId, array $relations = []): Collection;

    /**
     * @param int|null $regionId
     * @return Collection
     */
    public function getUsersByRegion(int $regionId = null): Collection;

    /**
     * @param UsersFilterDTO $usersFilterDTO
     * @return Collection
     */
    public function getUsers(UsersFilterDTO $usersFilterDTO): Collection;

    /**
     * @param UsersFilterDTO $usersFilterDTO
     * @return LengthAwarePaginator
     */
    public function getUsersByRegionAndCategory(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator;

    /**
     * @param UsersFilterDTO $usersFilterDTO
     * @return LengthAwarePaginator
     */
    public function getUsersByRegionAndSubCategory(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator;

    /**
     * @param UsersFilterDTO $usersFilterDTO
     * @return LengthAwarePaginator
     */
    public function getUsersForHomePage(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator;

    /**
     * @param UsersFilterDTO $usersFilterDTO
     * @return LengthAwarePaginator
     */
    public function getUsersByRegionAndAdvert(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator;

    /**
     * @param int $userId
     * @return Collection
     */
    public function getUserCities(int $userId): Collection;

    public function getUserData(int $id, array $relations = []): ?User;

    public function update(int $id, array $fields): ?User;
}
