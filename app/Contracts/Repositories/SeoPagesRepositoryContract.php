<?php

namespace App\Contracts\Repositories;

use App\Models\SeoPage;

interface SeoPagesRepositoryContract
{
    public function getSeoPageData(string $pageType): SeoPage;
}
