<?php

namespace App\Contracts\Repositories;

use App\DTO\CategoriesFilterDTO;
use App\DTO\UsersFilterDTO;
use App\Models\Category;
use Illuminate\Support\Collection;

interface CategoriesRepositoryContract
{
    public function getHomeCategories(UsersFilterDTO $usersFilterDTO, array $relations = []): Collection;

    public function getCategoryUrlInfoRegion(string $codeFromUrl): Category;

    public function getCategoryByCode(string $code): Category;

    public function getSubCategories(CategoriesFilterDTO $categoriesFilterDTO, array $relations = []): Collection;
}
