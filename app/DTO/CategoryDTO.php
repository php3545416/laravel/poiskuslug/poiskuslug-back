<?php

namespace App\DTO;

use App\Models\Advert;
use App\Models\Category;
use Illuminate\Support\Collection;

class CategoryDTO
{
    private Category $category;
    private ?Collection $subCategories = null;
    private ?Advert $advert = null;

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): static
    {
        $this->category = $category;
        return $this;
    }

    public function getSubCategory(): ?Category
    {
        return $this->subCategory;
    }

    public function setSubCategory(?Category $subCategory): static
    {
        $this->subCategory = $subCategory;
        return $this;
    }

    public function setAdvert(?Advert $advert): static
    {
        $this->advert = $advert;
        return $this;
    }

    public function getAdvert(): ?Advert
    {
        return $this->advert;
    }
}

