<?php

namespace App\DTO;

use App\Models\Advert;
use App\Models\Category;
use App\Models\Region;

class URLTreeDTO
{
    private Region $region;
    private Category $category;
    private ?Category $subCategory = null;
    private ?Advert $advert = null;

    public function getRegion(): Region
    {
        return $this->region;
    }

    public function setRegion(Region $region): static
    {
        $this->region = $region;
        return $this;
    }

    public function getCategory(): Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): static
    {
        $this->category = $category;
        return $this;
    }

    public function getSubCategory(): ?Category
    {
        return $this->subCategory;
    }

    public function setSubCategory(?Category $subCategory): static
    {
        $this->subCategory = $subCategory;
        return $this;
    }

    public function setAdvert(?Advert $advert): static
    {
        $this->advert = $advert;
        return $this;
    }

    public function getAdvert(): ?Advert
    {
        return $this->advert;
    }

    public function getUrl(): ?string
    {
        $url = '/' . $this->region->url . '/';
        $url .= $this->category->url . '/';
        $url .= $this->subCategory ? $this->subCategory->url . '/' : '';
        $url .= $this->advert ? $this->advert->url . '/' : '';

        return $url;
    }
}

