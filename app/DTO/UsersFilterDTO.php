<?php

namespace App\DTO;

class UsersFilterDTO
{
    private ?string $phone = null;
    private ?string $email = null;
    private ?string $name = null;
    private ?int $limit = null;
    private ?int $regionId = null;
    private ?int $categoryId = null;
    private ?int $subCategoryId = null;
    private ?int $advertId = null;
    private ?int $page = null;
    private ?int $perPage = null;
    private ?string $orderBy = null;
    private ?string $orderByDirection = null;
    private ?array $relations = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return UsersFilterDTO
     */
    public function setName(?string $name): static
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     * @return UsersFilterDTO
     */
    public function setEmail(?string $email): static
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     * @return UsersFilterDTO
     */
    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSubCategoryId(): ?string
    {
        return $this->subCategoryId;
    }

    /**
     * @param int|null $subCategoryId
     * @return UsersFilterDTO
     */
    public function setSubCategoryId(?int $subCategoryId): static
    {
        $this->subCategoryId = $subCategoryId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getAdvertId(): ?int
    {
        return $this->advertId;
    }

    /**
     * @param int|null $advertId
     * @return UsersFilterDTO
     */
    public function setAdvertId(?int $advertId): static
    {
        $this->advertId = $advertId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     * @return UsersFilterDTO
     */
    public function setLimit(?int $limit): static
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRegionId(): ?int
    {
        return $this->regionId;
    }

    /**
     * @param int|null $regionId
     * @return UsersFilterDTO
     */
    public function setRegionId(?int $regionId): static
    {
        $this->regionId = $regionId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCategoryId(): ?string
    {
        return $this->categoryId;
    }

    /**
     * @param int|null $categoryId
     * @return UsersFilterDTO
     */
    public function setCategoryId(?int $categoryId): static
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     * @return UsersFilterDTO
     */
    public function setPage(?int $page): static
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPerPage(): ?int
    {
        return $this->perPage;
    }

    /**
     * @param int|null $perPage
     * @return UsersFilterDTO
     */
    public function setPerPage(?int $perPage): static
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderBy(): ?string
    {
        return $this->orderBy;
    }

    /**
     * @param string|null $orderBy
     * @return UsersFilterDTO
     */
    public function setOrderBy(?string $orderBy): static
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOrderByDirection(): ?string
    {
        return $this->orderByDirection;
    }

    /**
     * @param string|null $orderByDirection
     * @return UsersFilterDTO
     */
    public function setOrderByDirection(?string $orderByDirection): static
    {
        $this->orderByDirection = $orderByDirection;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getRelations(): ?array
    {
        return $this->relations;
    }

    /**
     * @param array|null $relations
     * @return UsersFilterDTO
     */
    public function setRelations(?array $relations): static
    {
        $this->relations = $relations;
        return $this;
    }
}

