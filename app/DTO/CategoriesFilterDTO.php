<?php

namespace App\DTO;

class CategoriesFilterDTO
{
    private ?int $parentId = null;

    private ?int $regionId = null;

    public function getParentId(): ?int
    {
        return $this->parentId;
    }

    public function setParentId(?int $parentId): static
    {
        $this->parentId = $parentId;
        return $this;
    }

    public function getRegionId(): ?int
    {
        return $this->regionId;
    }

    public function setRegionId(?int $regionId): static
    {
        $this->regionId = $regionId;
        return $this;
    }
}

