<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Advert extends Model
{
    use HasFactory;

    protected $table = 'adverts';

    protected $casts = [
        'can_be_selected' => 'boolean',
    ];

    protected $fillable = [
        'name',
        'category_id',
        'favorite',
        'popular',
        'show_col',
        'url',
        'icon',
        'title',
        'h1',
        'description',
        'text',
        'name_bread',
        'master_1',
        'master_2',
        'master_3',
        'measure',
        'can_be_selected',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
