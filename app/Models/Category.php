<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Cache;


class Category extends Model
{
    use HasFactory;

    private static $RegionHasMany = 0;

    protected $casts = [
        'favorite' => 'boolean',
        'display_at_home' => 'boolean',
    ];

    protected $fillable = [
        'name',
        'parent_id',
        'url',
        'icon',
        'title',
        'h1',
        'description',
        'text',
        'name_bread',
        'master_1',
        'master_2',
        'master_3',
        'favorite',
        'popular',
        'show_col',
        'measure',
        'display_at_home',
    ];

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    public function adverts(): HasMany
    {
        return $this->hasMany(Advert::class);
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'category_user', 'sub_category_id', 'user_id');
    }

    public function usersByParent(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'category_user', 'category_id', 'user_id');
    }

    protected static function boot() {
        parent::boot();

        static::saving(function ($Category) {

            Cache::forget("get_current_category_{$Category->url}");
            Cache::forget("get_current_category_{$Category->id}");
            Cache::forget("GetCategoryURL_{$Category->id}");
        });
    }

    public static function GetAllCategories(int $parentId = 0): Collection
    {
        return Category::where('parent_id', $parentId)
            ->orderBy('name', 'asc')
            ->get()
            ->map(function (Category $parent) {
                $parent->subCategories = Category::where('parent_id', $parent->id)->with('adverts')->orderBy('name', 'asc')->get();
                return $parent;
        });
    }

    //Урл для подкатегории
    public static function GetSubCategoryURL($CategoryID, $RegionID = '')
    {
        if(!$RegionID OR $RegionID==''){
            $RegionID = 450;
        }
        $Category = array();
        $Region = Region::GetRegionURL($RegionID);
        if (Cache::has("get_current_category_{$CategoryID}")) {
            $SubCategory = Cache::get("get_current_category_{$CategoryID}");
        }else{
            $SubCategory = Category::where(['id' => $CategoryID])->first();
            Cache::put("get_current_category_{$CategoryID}",$SubCategory,3600);
        }

        if(!empty($SubCategory->parent_id)){
            if (Cache::has("get_current_category_{$SubCategory->parent_id}")) {
                $Category = Cache::get("get_current_category_{$SubCategory->parent_id}");
            }else{
                $Category = Category::where(['id' => $SubCategory->parent_id])->first();
                Cache::put("get_current_category_{$SubCategory->parent_id}",$Category,3600);
            }
        }

        return [
            'name' => ! empty($SubCategory->name) ?  $SubCategory->name : '',
            'slug'=> ! empty($SubCategory->url) ?  $SubCategory->url : '',
            'parent_name' => !empty($Category->name) ?  $Category->name : '',
            'url' =>  !empty($Category->url) ? route('region.subcategory', [
                'region' => $Region['url'],
                    'category' => ! empty($Category->url) ?  $Category->url : 0,
                    'subcategory' => ! empty($SubCategory->url) ?  $SubCategory->url : 0
            ]) : ''
        ];
    }

    //Список категорий со вложенностью
    public static function GetCategoryTree()
    {
        $Categories = self::GetAllCategories(0,0,1);
        foreach ($Categories as &$category) {
            $category['children'] = self::GetAllCategories($category->id,0,1);
            foreach ($category['children']  as &$scategory){
                $scategory['children'] = $scategory->adverts;
            }
        }
        return $Categories;
    }
}
