<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeoPage extends Model
{
    use HasFactory;

    protected $table = 'seo_settings';

    protected $guarded = [];
}
