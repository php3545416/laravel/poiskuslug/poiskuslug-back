<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class Region extends Model
{
    use HasFactory;

    protected static function boot() {
        parent::boot();

        static::saving(function ($Region) {
            Cache::forget("current_region_{$Region->url}");
            Cache::forget("current_region_{$Region->id}");
        });
    }

    protected $fillable = [
        'type',
        'name',
        'name_case',
        'url',
        'favorite',
        'parent_id',
        'public',
        'footer',
    ];

    public function usersByRegion(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'region_user', 'region_id', 'user_id');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'region_user','city_id', 'user_id');
    }

    public static function GetAllRegions($WithCounts = 0, $Chunk=0, $isAdmin = 0)
    {
        if($WithCounts==1){
            if($Chunk){
                $RegionsCount = Region::select(['regions.id'])->where(['type'=> 1,'public'=>1])->orderBy('name', 'asc')->count();

                $Regions = Region::select(['regions.id','regions.type','regions.public','regions.name','regions.url','regions.favorite','regions.parent_id'])->where(['type'=> 1]);

                if($isAdmin==0){
                    $Regions->where('public',1);
                }
                $Regions = $Regions->orderBy('name', 'asc')->get()->chunk(ceil($RegionsCount/3));

            }else{
                $Regions = Region::select(['regions.id','regions.name','regions.public','regions.url','regions.favorite','regions.parent_id'])->where(['type'=> 1]);
                if($isAdmin==0){
                    $Regions->where('public',1);
                }
                $Regions = $Regions->orderBy('name', 'asc')->get();
            }
        }else{
            $Regions = Region::where(['type'=> 1])->orderBy('name', 'asc')->get();
        }
        return $Regions;

    }

    public static function GetAllCitys($RegionID, $WithCounts = 0,$Chunk=0,$isAdmin = 0)
    {
        if ($WithCounts == 1) {
            if($Chunk){

                $CitysCount = Region::select(['regions.id'])->where(['type'=>2,'public'=>1,'parent_id' => $RegionID])->orderBy('name', 'asc')->count();

                $Citys = Region::select(['regions.id','regions.public','regions.parent_id','regions.type','regions.name','regions.url','regions.favorite','regions.parent_id'])->where(['type'=>2,'public'=>1,'parent_id' => $RegionID])->groupBy('regions.id')->orderBy('name', 'asc')->get()->chunk(ceil($CitysCount/3));;
            }else{

                $Citys = Region::select(['regions.id','regions.public','regions.parent_id','regions.type','regions.name','regions.url','regions.favorite','regions.parent_id'])->where(['type'=>2,'parent_id' => $RegionID])->groupBy('regions.id')->orderBy('name', 'asc')->get();
            }
        } else {
            $Citys = Region::where(['type' => 2, 'parent_id' => $RegionID])->orderBy('name', 'asc')->get();
        }
        return $Citys;

    }

    public static function GetRegionURL($RegionID)
    {
        if (Cache::has("current_region_$RegionID")) {
            $Region = Cache::get("current_region_$RegionID");
        }else{
            $Region = Region::where('id', $RegionID)->first();
            Cache::put("current_region_$RegionID", $Region, 3600);
        }

        return array('name' => $Region->name, 'url' => '/' . $Region->url);
    }

    public static function GetRegionByURL($Url){
        if (Cache::has("current_region_$Url")) {
            $Region = Cache::get("current_region_$Url");
        }else{
            $Region = Region::where('url', $Url)->first();
            Cache::put("current_region_$Url", $Region, 3600);
        }
        return $Region;
    }
}
