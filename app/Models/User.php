<?php

namespace App\Models;

use App\Enums\Sex;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @method static create(array $array)
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'user_type',
        'phone',
        'password',
        'hash',
        'user_type',
        'confirm_phone',
        'confirm_email',
        'last_login',
        'last_phone_call',
        'views_total',
        'views_today',
        'status',
        'photo',
        'work_experience',
        'discount_text',
        'discount',
        'about_text',
        'last_profile_up_at',
        'birthday',
        'sex',
        'email_verified_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_profile_up_at' => 'datetime',
    ];

    public static function GetUsersList($Filter = array())
    {
        $Users = User::where('user_type', '>=', 0);

        //Поиск по ID
        if (isset($Filter['id'])) {
            $Users = $Users->where('id', $Filter['id']);
        }

        //Поиск по E-mail
        if (isset($Filter['email'])) {
            $Users = $Users->orwhere('email', 'LIKE', '%' . $Filter['email'] . '%');
        }

        return $Users->orderBy('id', 'DESC')->paginate(50);
    }

    public function adverts(): BelongsToMany
    {
        return $this->belongsToMany(Advert::class)->withPivot('price', 'discount', 'discount_text');
    }

    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class, 'category_user', 'user_id', 'sub_category_id');
    }

    public function regions(): BelongsToMany
    {
        return $this->belongsToMany(Region::class, 'region_user', 'user_id', 'city_id');
    }

    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }
}
