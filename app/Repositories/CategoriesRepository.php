<?php

namespace App\Repositories;

use App\Contracts\Repositories\CategoriesRepositoryContract;
use App\DTO\CategoriesFilterDTO;
use App\DTO\UsersFilterDTO;
use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CategoriesRepository implements CategoriesRepositoryContract
{
    public function __construct(private readonly Category $model)
    {
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function getHomeCategories(UsersFilterDTO $usersFilterDTO, array $relations = []): Collection
    {
        return $this->getModel()->newQuery()
            ->where(['parent_id' => '0'])
            ->whereHas('usersByParent')
            ->orWhere('display_at_home',true)
            ->orderBy('name')
            ->with($usersFilterDTO->getRelations())
            ->get();
    }

    public function getHomeRegionsCategories(UsersFilterDTO $usersFilterDTO, array $relations = []): Collection
    {
        return $this->getModel()->newQuery()
            ->where(['parent_id' => '0'])
            ->whereHas('usersByParent.regions', function ($query) use ($usersFilterDTO) {
                $query->where('city_id', $usersFilterDTO->getRegionId());
            })
            ->orWhere('display_at_home',true)
            ->with($usersFilterDTO->getRelations())
            ->orderBy('name')
            ->get();
    }

    public function getCategoryUrlInfoRegion(string $codeFromUrl): Category
    {
        return Cache::remember(
            "current_category_{$codeFromUrl}",
            Carbon::now()->addHour(),
            function () use ($codeFromUrl) {
                return $this->getModel()->newQuery()
                    ->where(['url' => $codeFromUrl])
                    ->first();
        });
    }

    /**
     * @return Model
     */
    public function getCategoryByCode(string $code): Category
    {
        return $this->getModel()->newQuery()
            ->where(['url' => $code])
            ->first();
    }

    public function getSubCategories(CategoriesFilterDTO $categoriesFilterDTO, array $relations = []): Collection
    {
        return $this->getModel()->newQuery()
            ->where(['parent_id' => $categoriesFilterDTO->getParentId(), 'favorite' => true])
            ->whereHas('users.regions', function ($query) use ($categoriesFilterDTO) {
                $query->where('city_id', $categoriesFilterDTO->getRegionId());
            })
            ->orderBy('name')
            ->get();
    }

}
