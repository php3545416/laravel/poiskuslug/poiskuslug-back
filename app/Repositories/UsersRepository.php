<?php

namespace App\Repositories;

use App\Contracts\Repositories\UsersRepositoryContract;
use App\DTO\UsersFilterDTO;
use App\Models\User;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class UsersRepository implements UsersRepositoryContract
{
    public function __construct(private readonly User $model)
    {
    }

    public function getModel(): User
    {
        return $this->model;
    }

    public function getUserById(int $id): User
    {
        return $this->getModel()::find($id);
    }

    public function getUserAdverts(int $userId, array $relations = []): Collection
    {
        return $this->getModel()
            ->newQuery()
            ->where(['id' => $userId])
            ->first()
            ->adverts
            ->load('category');
    }

    public function getUserCategories(int $userId, array $relations = []): Collection
    {
        return $this->getModel()
            ->newQuery()
            ->with(array_merge(['categories'], $relations))
            ->where(['id' => $userId])
            ->get();
    }

    public function getUsersByRegion(int $regionId = null, array $relations = [], int $limit = 20): Collection
    {
        return $this->getModel()::with(array_merge(['regions' => function (Builder $query) use ($regionId) {
            $query->where('city_id', $regionId);
        }, 'categories', 'adverts'], $relations))->limit($limit)->get();
    }

    public function getUsers(
        UsersFilterDTO $usersFilterDTO,
        array $relationsFilter = []
    ): Collection {
        return $this->getModel()::whereHas('regions', function (Builder $query) use ($usersFilterDTO) {
                $query->where('city_id', $usersFilterDTO->getRegionId());
            })->whereHas('categories', function (Builder $query) use ($usersFilterDTO) {
                $query->where('category_id', $usersFilterDTO->getCategoryId());
            })
            ->limit($usersFilterDTO->getLimit())
            ->get();
    }

    public function getUsersByRegionAndCategory(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator
    {
        return $this->getModel()
            ->newQuery()
            ->whereHas('regions', function (Builder $query) use ($usersFilterDTO) {
                $query->where('city_id', $usersFilterDTO->getRegionId());
            })
            ->whereHas('categories', function (Builder $query) use ($usersFilterDTO) {
                $query->where('category_id', $usersFilterDTO->getCategoryId());
            })
            ->with($usersFilterDTO->getRelations())
            ->orderBy($usersFilterDTO->getOrderBy(), $usersFilterDTO->getOrderByDirection())
            ->limit($usersFilterDTO->getLimit())
            ->paginate($usersFilterDTO->getPerPage(), ['*'], 'page', $usersFilterDTO->getPage());
    }

    public function getUsersByRegionAndSubCategory(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator
    {
        return $this->getModel()
            ->newQuery()
            ->whereHas('regions', function (Builder $query) use ($usersFilterDTO) {
                $query->where('city_id', $usersFilterDTO->getRegionId());
            })
            ->whereHas('categories', function (Builder $query) use ($usersFilterDTO) {
                $query->where('sub_category_id', $usersFilterDTO->getSubCategoryId());
            })
            ->with($usersFilterDTO->getRelations())
            ->orderBy($usersFilterDTO->getOrderBy(), $usersFilterDTO->getOrderByDirection())
            ->limit($usersFilterDTO->getLimit())
            ->paginate($usersFilterDTO->getPerPage(), ['*'], 'page', $usersFilterDTO->getPage());
    }

    public function getUsersForHomePage(
        UsersFilterDTO $usersFilterDTO
    ): LengthAwarePaginator {
        return $usersFilterDTO->getRegionId() ?
            $this->getModel()
                ->newQuery()
                ->whereHas('regions', function (Builder $query) use ($usersFilterDTO) {
                    $query->where('city_id', $usersFilterDTO->getRegionId());
                })
                ->has('categories')
                ->with($usersFilterDTO->getRelations())
                ->limit($usersFilterDTO->getLimit())
                ->orderBy($usersFilterDTO->getOrderBy(), $usersFilterDTO->getOrderByDirection())
                ->paginate($usersFilterDTO->getPerPage(), ['*'], 'page', $usersFilterDTO->getPage()) :
            $this->getModel()
                ->newQuery()
                ->has('categories')
                ->with($usersFilterDTO->getRelations())
                ->limit($usersFilterDTO->getLimit())
                ->orderBy($usersFilterDTO->getOrderBy(), $usersFilterDTO->getOrderByDirection())
                ->paginate($usersFilterDTO->getPerPage(), ['*'], 'page', $usersFilterDTO->getPage());
    }

    public function getUsersByRegionAndAdvert(UsersFilterDTO $usersFilterDTO): LengthAwarePaginator
    {
        return $this->getModel()
            ->newQuery()
            ->whereHas('regions', function (Builder $query) use ($usersFilterDTO) {
                $query->where('city_id', $usersFilterDTO->getRegionId());
            })->whereHas('adverts', function (Builder $query) use ($usersFilterDTO) {
                $query->where('advert_id', $usersFilterDTO->getAdvertId());
            })
            ->with($usersFilterDTO->getRelations())
            ->limit($usersFilterDTO->getLimit())
            ->orderBy($usersFilterDTO->getOrderBy(), $usersFilterDTO->getOrderByDirection())
            ->paginate($usersFilterDTO->getPerPage(), ['*'], 'page', $usersFilterDTO->getPage());
    }

    /**
     * @return Model|null
     */
    public function getUserByPhone(string $phone): ?User
    {
        return $this->getModel()
            ->newQuery()
            ->where('phone' , $phone)
            ->first();
    }

    public function getUserCities(int $userId): Collection
    {
        return $this->getModel()
            ->newQuery()
            ->where('id', $userId)
            ->first()
            ->regions;
    }

    /**
     * @return Model|null
     */
    public function getUserData(int $id, array $relations = []): ?User
    {
        return $this->getModel()
            ->newQuery()
            ->with($relations)
            ->find($id);
    }

    public function update(int $id, array $fields): ?User
    {
        return tap(
            $this->getModel()->newQuery()->find($id),
            fn(Model|null $user) => $user?->update($fields)
        );
    }
}
