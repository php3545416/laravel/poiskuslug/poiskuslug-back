<?php

namespace App\Repositories;

use App\Contracts\Repositories\AdvertsRepositoryContract;
use App\Models\Advert;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class AdvertsRepository implements AdvertsRepositoryContract
{
    public function __construct(private readonly Advert $model)
    {
    }

    public function getModel(): Advert
    {
        return $this->model;
    }

    /**
     * @return Model
     */
    public function getAdvertByCode(string $code): Advert
    {
        return $this->getModel()->newQuery()->where(['url' => $code])->first();
    }

    public function getPopularAdvertsByCategory(int $categoryId, int $regionId): Collection
    {
        return $this->getModel()->newQuery()
            ->where(['category_id' => $categoryId, 'popular' => true])
            ->whereHas('users.regions', function ($query) use ($regionId) {
                    $query->where('city_id', $regionId);
            })
            ->get();
    }

    public function getSidePanelAdvertsByCategory(int $categoryId, int $regionId): Collection
    {
        return $this->getModel()->newQuery()
            ->where([
                'category_id' => $categoryId,
                'show_col' => true
            ])
            ->whereHas('users.regions', function ($query) use ($regionId) {
                $query->where('city_id', $regionId);
            })
            ->get();
    }

    /**
     * @return Model
     */
    public function getById(int $id): Advert
    {
        return $this->getModel()->newQuery()->where('id', $id)->first();
    }

    /**
     * @return Model
     */
    public function create(array $fields): Advert
    {
        return $this->getModel()->newQuery()->create($fields);
    }

    /**
     * @return Model
     */
    public function update(int $id, array $fields): Advert
    {
        return tap(
            $this->getModel()->newQuery()->findOrFail($id),
            fn(Advert $advert) => $advert->update($fields)
        );
    }

    /**
     * @return Model
     */
    public function delete(int $id): int
    {
        return $this->getModel()->newQuery()->findOrFail($id)->delete();
    }
}
