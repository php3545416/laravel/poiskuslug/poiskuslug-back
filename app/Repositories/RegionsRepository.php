<?php

namespace App\Repositories;

use App\Contracts\Repositories\RegionsRepositoryContract;
use App\Models\Region;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class RegionsRepository implements RegionsRepositoryContract
{
    public function __construct(private readonly Region $model)
    {
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function getRegionByURL(string $paramFromURL): Region
    {
        return Cache::remember("current_region_{$paramFromURL}", Carbon::now()->addHours(1), fn () =>
            $this->getModel()->where('url', $paramFromURL)->first()
        );
    }

    public function getCity(string $code): Region
    {
        return $this->getModel()->where(['url' => $code])->first();
    }

    public function getRegionByCode(string $code): Region
    {
        return $this->getModel()->where(['url' => $code, 'type' => 1, 'public' => 1])->first();
    }

    public function getRegionById(int $id): Region
    {
        return $this->getModel()->where(['id' => $id])->first();
    }

    public function getAllRegions(int $chunkCount = 350, bool $isOnlyPublic = true): Collection
    {
        return $this->getModel()::select([
            'regions.id',
            'regions.type',
            'regions.public',
            'regions.name',
            'regions.url',
            'regions.favorite',
            'regions.parent_id'
        ])
            ->withCount('usersByRegion as users_count')
            ->where(['type' => 1, 'public' => $isOnlyPublic])
            ->orderBy('name', 'asc')
            ->get();
    }

    public function getRegionCities(int $regionId, int $chunkCount = 10): Collection
    {
        return $this->getModel()::select([
            'regions.id',
            'regions.public',
            'regions.parent_id',
            'regions.type',
            'regions.name',
            'regions.url',
            'regions.favorite',
        ])
            ->withCount('users as users_count')
            ->where(['type' => 2, 'public' => 1, 'parent_id' => $regionId])
            ->orderBy('name', 'asc')
            ->orderBy('name', 'asc')
            ->get();
    }
}
