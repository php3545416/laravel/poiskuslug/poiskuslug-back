<?php

namespace App\Repositories;

use App\Contracts\Repositories\SeoPagesRepositoryContract;
use App\Models\SeoPage;
use Illuminate\Database\Eloquent\Model;

class SeoPagesRepository implements SeoPagesRepositoryContract
{
    public function __construct(private readonly SeoPage $model)
    {
    }

    public function getModel(): Model
    {
        return $this->model;
    }

    public function getSeoPageData(string $pageType): SeoPage
    {
        return $this->getModel()::where(['page_type' => $pageType])->first();
    }
}
