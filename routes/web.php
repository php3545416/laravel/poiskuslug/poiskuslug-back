<?php

use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\AdvertsController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\RegionsController;
use App\Http\Controllers\Admin\SeoController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Profile\UserController as ProfileUserController;
use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/test', function () {
    Auth::login(User::where('email', 'vitekskrip@gmail.com')->first());
});

Route::group(['prefix' => 'api', 'namespace' => 'App\Http\Controllers\Api'], function () {
    Route::post('/', 'HomeController@index')->name('api.home.page');
    Route::post('/categoryPage/', 'ListingsController@categoryPage')->name('api.category.page');
    Route::post('/subCategoryPage/', 'ListingsController@subCategoryPage')->name('api.subCategory.page');
    Route::post('/advertPage/', 'ListingsController@advertPage')->name('api.advert.page');
    Route::get('/profile/up/', 'Profile\ProfileController@upProfile')->name('api.profile.up');
});

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/contacts/', [App\Http\Controllers\HomeController::class, 'contacts'])->name('contacts');

Route::get('/userterms/', [App\Http\Controllers\HomeController::class, 'userterms'])->name('userterms');
Route::get('/news/', [App\Http\Controllers\HomeController::class, 'news'])->name('News');
Route::get('/userpolitics/', [App\Http\Controllers\HomeController::class, 'userpolitics'])->name('userpolitics');
Route::post('/contacts/', [App\Http\Controllers\HomeController::class, 'contactsSend'])->name('contacts.send');
Route::get('/region/', [App\Http\Controllers\RegionsController::class, 'showRegionList'])->name('region.index');
Route::get('/wishlist/', [App\Http\Controllers\AdvertsController::class, 'wishList'])->name('wishlist');
Route::get('/region/{region}/', [App\Http\Controllers\RegionsController::class, 'showRegionCityList'])->name('region.city');

Route::get('/system/cachemanager/', [App\Http\Controllers\CacheManager::class, 'advertsCountManager']);
Route::get('/user/{id}/', [App\Http\Controllers\UserController::class, 'currentUser'])->name('user.current');

//Админ
Route::group(['middleware' => ['auth.admin']], function () {
    Route::get('/pult/', [AdminController::class, 'index'])->name('admin.index');
    Route::get('/logout/', [UserController::class, 'logout'])->name('user.logout');
    Route::post('/logout/', [UserController::class, 'logout'])->name('user.logout');
    Route::get('/pult/items/import-info/', [AdminController::class, 'itemsListImportUploadInfo']);
    Route::post('/pult/items/import/', [AdminController::class, 'itemsListImportUpload'])->name('admin.items.import');
    Route::get('/pult/pages/list/', [AdminController::class, 'list'])->name('admin.pages.list');
    Route::get('/pult/contacts/list/', [AdminController::class, 'listContacts'])->name('admin.contacts.list');
    Route::get('/pult/contacts-adverts/list/', [AdminController::class, 'listAdvertsContacts'])->name('admin.contacts-adverts.list');


    /** Categories Manager */

    Route::group(['prefix' => '/pult/categories'], function () {
        Route::get('/list/', [CategoriesController::class, 'list'])->name('admin.category.list');
        Route::get('/edit/{id}/', [CategoriesController::class, 'edit'])->name('admin.category.edit');
        Route::post('/delete/{id}/', [CategoriesController::class, 'delete'])->name('admin.category.delete');
        Route::get('/delete/{id}/', [CategoriesController::class, 'delete'])->name('admin.category.delete');
        Route::post('/edit/', [CategoriesController::class, 'save'])->name('admin.category.save');
    });


    /** Adverts Manager */

    Route::group(['prefix' => '/pult/adverts'], function () {
        Route::get('/', [AdvertsController::class, 'create'])->name('admin.advert.create');
        Route::post('/', [AdvertsController::class, 'store'])->name('admin.advert.store');
        Route::get('/{id}/', [AdvertsController::class, 'edit'])->name('admin.advert.edit');
        Route::post('/{id}/',[AdvertsController::class, 'update'])->name('admin.advert.update');
        Route::get('/{id}/delete/',[AdvertsController::class, 'delete'])->name('admin.advert.delete');
        Route::delete('/{id}/delete/',[AdvertsController::class, 'delete'])->name('admin.advert.delete');
    });

    /** Regions Manager */

    Route::group(['prefix' => '/pult/region'], function () {

        Route::get('/list/', [RegionsController::class, 'list'])->name('admin.region.list');
        Route::get('/list/{region}/', [RegionsController::class, 'listCity'])->name('admin.region.list.city');
        Route::get('/edit/{id}/', [RegionsController::class, 'edit'])->name('admin.region.edit');
        Route::post('/delete/{id}/', [RegionsController::class, 'delete'])->name('admin.region.delete');
        Route::get('/delete/{id}/', [RegionsController::class, 'delete'])->name('admin.region.delete');
        Route::post('/edit/', [RegionsController::class, 'save'])->name('admin.region.save');
    });

    /** Users Manager */

    Route::group(['prefix' => '/pult/users'], function () {
        Route::get('/list/', [UsersController::class, 'list'])->name('admin.users.list');
        Route::get('/edit/{id}/', [UsersController::class, 'edit'])->name('admin.users.edit');
        Route::post('/edit/', [UsersController::class, 'save'])->name('admin.users.save');
        Route::post('/users/delete/{id}/', [UsersController::class, 'delete'])->name('admin.users.delete');
    });

    /** Seo Manager */

    Route::group(['prefix' => '/pult/seo'], function () {
        Route::get('/edit/', [SeoController::class, 'edit'])->name('admin.seo.edit');
        Route::post('/save/', [SeoController::class, 'save'])->name('admin.seo.save');
        Route::post('/banners/save/', [SeoController::class, 'bannersSave'])->name('admin.banners.save');
    });
});

Route::group([], function () {
    Route::post('/register/', [AuthController::class, 'register'])->name('register');
    Route::post('/makeCall/', [AuthController::class, 'makeCall'])->name('makeCall');
    Route::post('/login/', [AuthController::class, 'login'])->name('login');
    Route::post('/email/send-verification/', [AuthController::class, 'sendEmailVerification'])->middleware(['auth'])->name('email-verification.send');
    Route::get('/email/verify/{id}/{hash}/', [AuthController::class, 'verifyEmail'])->middleware(['validate.email.verify.url'])->name('email-verification.verify');
});

Route::middleware('auth')->group(function () {
    Route::post('/logout/', [AuthController::class, 'destroy'])->name('user.logout');
    Route::get('/logout/', [AuthController::class, 'destroy'])->name('user.logout');

    /** Profile */

    Route::get('/profile/', [App\Http\Controllers\UserController::class, 'userProfile'])->name('user.profile');
    Route::get('/profile/about/', [App\Http\Controllers\UserController::class, 'userAboutPage'])->name('user.profile.about.view');
    Route::post('/profile/about-info/', [App\Http\Controllers\UserController::class, 'saveUserAboutInfo'])->name('user.profile.about.save');
    Route::post('/profile/about/', [App\Http\Controllers\UserController::class, 'userAbout'])->name('user.about');
    Route::post('/profile/discount/', [App\Http\Controllers\UserController::class, 'userDiscount'])->name('user.discount');
    Route::get('/profile/portfolio/', [App\Http\Controllers\UserController::class, 'userPortfolio'])->name('user.profile.portfolio');
    Route::post('/profile/portfolio/upload/', [App\Http\Controllers\UserController::class, 'uploadPortfolio'])->name('user.profile.portfolio.upload');
    Route::post('/profile/upload-photo/', [App\Http\Controllers\UserController::class, 'uploadUserPhoto'])->name('user.upload.photo');
    Route::get('/profile/regions/', [App\Http\Controllers\UserController::class, 'userRegionsForm'])->name('user.profile.regions');
    Route::post('/profile/regions/', [App\Http\Controllers\UserController::class, 'userRegions'])->name('user.regions');


    /** Profile Adverts */

    Route::get('/profile/adverts/', [ProfileUserController::class, 'index'])->name('user.profile.adverts');
    Route::post('/profile/subCategories/', [ProfileUserController::class, 'addSubCategories'])->name('user.profile.add.subCategories');
    Route::post('/profile/advert/price/', [ProfileUserController::class, 'addAdvertPrice'])->name('user.profile.advert.price');
    Route::post('/profile/set-session-profile/', [ProfileUserController::class, 'setSessionProfileParam']);

    Route::post('/profile/settings/', [App\Http\Controllers\UserController::class, 'UserSettings'])->name('user.settings');
});


Route::prefix('api')->group(function () {
        Route::delete('/portfolio/', [App\Http\Controllers\UserController::class, 'deletePortfolio']);
        Route::post('/get-citys-by-region/', [App\Http\Controllers\AdvertsController::class, 'getCitysByRegion']);
        Route::post('/update-wish/', [App\Http\Controllers\AdvertsController::class, 'updateWish']);
        Route::post('/update-advert/', [App\Http\Controllers\AdvertsController::class, 'updateAdvert']);
        Route::post('/delete-advert/', [App\Http\Controllers\AdvertsController::class, 'deleteAdvert']);
        Route::post('/close-advert/', [App\Http\Controllers\AdvertsController::class, 'closeAdvert']);
        Route::post('/getphone/', [App\Http\Controllers\AdvertsController::class, 'getphone']);
        Route::post('/open-advert/', [App\Http\Controllers\AdvertsController::class, 'openAdvert']);
        Route::post('/get-categories-by-parent/', [App\Http\Controllers\AdvertsController::class, 'getCategoriesByParent']);
    }
);

Route::group(['prefix' => '{region}', 'middleware' => 'region'], function () {
    Route::get('/', [\App\Http\Controllers\RegionsController::class, 'index'])->name('advert.index');
    Route::get('/{category}/', [App\Http\Controllers\AdvertsController::class, 'categoryPage'])->name('region.category');
    Route::get('/{category}/{subcategory}/', [App\Http\Controllers\AdvertsController::class, 'subCategoryPage'])->name('region.subcategory');
    Route::get('/{category}/{subcategory}/{advert}/', [App\Http\Controllers\AdvertsController::class, 'currentAdvert'])->name('advert.current');
});
