<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'user_type' => fake()->randomElement([0, 1, 2]),
            'confirm_phone' => fake()->boolean(90),
            'confirm_email' => $bool = fake()->boolean(),
            'status' => fake()->boolean(90),
            'phone' => fake()->phoneNumber(),
            'photo' => fake()->imageUrl(),
            'last_login' => fake()->dateTime(),
            'work_experience' => fake()->numberBetween(0, 10),
            'about_text' => fake()->text(),
            'sex' => fake()->randomElement(['M', 'W']),
            'birthday' => fake()->dateTimeThisCentury(),
            'email' => $bool ? $this->faker->unique()->safeEmail() : null,
            'email_verified_at' => $bool ? now() : null,
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
