<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('region_user', function (Blueprint $table) {
            $table->unsignedInteger('city_id');
            $table->index(['city_id', 'user_id']);
        });

        $inserts = DB::table('region_user')->get();
        $insertsToArray = [];
        foreach ($inserts as $insert) {
            $insertsToArray[] = [
                'user_id' => $insert->user_id,
                'city_id' => $insert->region_id,
                'region_id' => \App\Models\Region::where('id', $insert->region_id)->first()->parent_id
            ];
        }

        DB::table('region_user')->insert($insertsToArray);
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('region_user', function (Blueprint $table) {
            $table->dropColumn('city_id');
            $table->dropIndex(['city_id', 'user_id']);
        });
    }
};
