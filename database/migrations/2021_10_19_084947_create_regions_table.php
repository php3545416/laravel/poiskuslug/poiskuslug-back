<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regions', function (Blueprint $table) {
            $table->id();

            $table->integer('type')->nullable();
            $table->string('name')->nullable();
            $table->string('name_case')->nullable();
            $table->string('url')->nullable();
            $table->boolean('favorite')->nullable();
            $table->boolean('footer')->nullable();
            $table->boolean('public')->nullable();
            $table->timestamps();
            $table->integer('parent_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regions');
    }
}
