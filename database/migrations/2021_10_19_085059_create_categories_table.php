<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->string('name');
            $table->integer('parent_id')->nullable();
            $table->boolean('favorite')->default(false);
            $table->boolean('footer')->default(false);
            $table->boolean('name_adv')->default(false);
            $table->boolean('show_short_description')->default(false);
            $table->boolean('popular')->default(false);
            $table->boolean('show_col')->default(false);
            $table->string('url')->nullable();
            $table->string('icon')->nullable();
            $table->string('title')->nullable();
            $table->string('h1')->nullable();
            $table->string('description')->nullable();
            $table->text('text')->nullable();
            $table->string('name_bread')->nullable();
            $table->text('master_1')->nullable();
            $table->text('master_2')->nullable();
            $table->text('master_3')->nullable();
            $table->text('measure')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
