<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('footer');
            $table->dropColumn('name_adv');
            $table->dropColumn('show_short_description');
        });
        Schema::table('adverts', function (Blueprint $table) {
            $table->dropColumn('footer');
            $table->dropColumn('name_adv');
            $table->dropColumn('show_short_description');
        });
    }
};
