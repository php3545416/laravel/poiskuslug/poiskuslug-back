<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advert_user', function (Blueprint $table) {
            $table->id();

            $table->foreignId('advert_id')
                ->references('id')
                ->on('adverts')
                ->cascadeOnDelete();

            $table->foreignId('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();

            $table->integer('price')->nullable();
            $table->smallInteger('discount')->nullable();
            $table->text('discount_text')->nullable();

            $table->index(['user_id', 'advert_id']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advert_user');
    }
}
